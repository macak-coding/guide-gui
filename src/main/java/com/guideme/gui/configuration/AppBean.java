package com.guideme.gui.configuration;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.catalina.Context;
import org.apache.tomcat.util.descriptor.web.SecurityCollection;
import org.apache.tomcat.util.descriptor.web.SecurityConstraint;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;

import freemarker.template.TemplateModelException;

@Configuration
public class AppBean {
	
	@Value("${cloudinary.cloud-name}")
	String cloudName;
	
	@Value("${cloudinary.api-key}")
	String apiKey;
	
	@Value("${cloudinary.api-secret}")
	String apiSecret;
	
	@Bean
	public RestTemplate getRestTemplateBean(RestTemplateBuilder builder){
		 return builder.basicAuthorization("ThaQ$z8spe", "cRExURaMUV8spamaPUM726@Am+32+#at")
				 .defaultMessageConverters().build();
	}
	
	
	
	/*
	 * Bean credential ke Cloudinary
	 * 
	 * */
	
	@Bean
	public Cloudinary getCloudinaryBean(){
		Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap(
			"cloud_name", cloudName,
			"api_key", apiKey,
			"api_secret", apiSecret
		)); 
		
		return cloudinary;
	}
	
	
	
	/*
	 * Method untuk redirect HTTP ke HTTPS
	 * 
	 * */
	
	/*@Bean
	TomcatEmbeddedServletContainerFactory getTomcatEmbeddedServletContainerFactory(){
		TomcatEmbeddedServletContainerFactory tomcat = new TomcatEmbeddedServletContainerFactory() {
		    @Override
		    protected void postProcessContext(Context context) {
		        SecurityConstraint securityConstraint = new SecurityConstraint();
		        securityConstraint.setUserConstraint("CONFIDENTIAL");
		        SecurityCollection collection = new SecurityCollection();
		        collection.addPattern("/*");
		        securityConstraint.addCollection(collection);
		        context.addConstraint(securityConstraint);
		    }
		};
		
		return tomcat;
	}*/
	
}
