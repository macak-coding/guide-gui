package com.guideme.gui.configuration;

import org.apache.log4j.Logger;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.boot.actuate.audit.listener.AuditApplicationEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;

@Component
public class AuthAttemptLogger {
	private static final Logger logger = Logger.getLogger(AuthAttemptLogger.class);
	
	@EventListener
	public void auditEventHappened(AuditApplicationEvent auditApplicationEvent) {

		AuditEvent auditEvent = auditApplicationEvent.getAuditEvent();
		logger.info("Principal " + auditEvent.getPrincipal() + " - " + auditEvent.getType());
		logger.info("Timestamp " + auditEvent.getTimestamp());
		
		if(!auditEvent.getPrincipal().equalsIgnoreCase("anonymousUser")){
			WebAuthenticationDetails details = (WebAuthenticationDetails) auditEvent.getData().get("details");
			logger.info("Remote IP address: " + details.getRemoteAddress());
			logger.info("Session Id: " + details.getSessionId());
		}
		
	}
}
