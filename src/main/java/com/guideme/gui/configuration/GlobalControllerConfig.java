package com.guideme.gui.configuration;

import java.security.Principal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.guideme.common.entity.User;
import com.guideme.gui.model.UserDetailCust;


@ControllerAdvice
public class GlobalControllerConfig {
	static Logger logger = Logger.getLogger(GlobalControllerConfig.class);
	
	private MessageSource messageSource;
	
	@Autowired
	public GlobalControllerConfig(MessageSource messageSource){
		this.messageSource = messageSource;
	}
	
	@ModelAttribute("global-variable")
	public void getGlobalAttributes(@AuthenticationPrincipal UserDetailCust user, 
			Model model, HttpSession session, Locale locale){
		
		int tahun = Calendar.getInstance().get(Calendar.YEAR);
		
		//session.setMaxInactiveInterval(30*60); /* Setting timeout session */
		model.addAttribute("principal", user);
		model.addAttribute("title", "Ohmyguide Indonesia");
		model.addAttribute("tahun", String.valueOf(tahun));
		model.addAllAttributes(constructLocales(locale));
	}
	
	private Map<String, String>constructLocales(Locale locale){
		Map<String, String> locales = new HashMap<>();
		
		locales.put("headerSmallTextLogin", messageSource.getMessage("header.small.text.login", null, locale));
		locales.put("headerSmallTextLogout", messageSource.getMessage("header.small.text.logout", null, locale));
		locales.put("headerSmallTextDaftar", messageSource.getMessage("header.small.text.daftar", null, locale));
		
		locales.put("headerMenuHome", messageSource.getMessage("header.menu.home", null, locale));
		locales.put("headerMenuSearch", messageSource.getMessage("header.menu.search", null, locale));
		locales.put("headerMenuJalanBareng", messageSource.getMessage("header.menu.jalan-bareng", null, locale));
		
		locales.put("footerWideSekilasTitle", messageSource.getMessage("footer.wide.sekilas.title", null, locale));
		locales.put("footerWideSekilasContent", messageSource.getMessage("footer.wide.sekilas.content", null, locale));
		locales.put("footerWideSekilasSelengkapnya", messageSource.getMessage("footer.wide.sekilas.selengkapnya", null, locale));
		locales.put("footerWideSubscribeTitle", messageSource.getMessage("footer.wide.subscribe.title", null, locale));
		locales.put("footerWideSubscriptionContent1", messageSource.getMessage("footer.wide.subscription.content1", null, locale));
		locales.put("footerWideSubscriptionContent2", messageSource.getMessage("footer.wide.subscription.content2", null, locale));
		locales.put("footerWideTopicTitle", messageSource.getMessage("footer.wide.topic.title", null, locale));
		locales.put("footerWideTopicTips", messageSource.getMessage("footer.wide.topic.tips", null, locale));
		locales.put("footerWideTopicSec", messageSource.getMessage("footer.wide.topic.sec", null, locale));
		locales.put("footerWideTopicSiteMap", messageSource.getMessage("footer.wide.topic.site-map", null, locale));
		locales.put("footerWideTopicAbout", messageSource.getMessage("footer.wide.topic.about", null, locale));
		locales.put("footerWideTopicContact", messageSource.getMessage("footer.wide.topic.contact", null, locale));
		locales.put("footerWideTopicPolicy", messageSource.getMessage("footer.wide.topic.policy", null, locale));
		
		return locales;
	}
}
