package com.guideme.gui.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.event.AbstractAuthenticationEvent;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

@Configuration
@EnableWebSecurity
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter implements ApplicationListener<AbstractAuthenticationEvent>{
	private static Logger logger = LoggerFactory.getLogger(SpringSecurityConfig.class);

	@Autowired
	private UserDetailsService userDetailsService;
	
	@Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
            
            /* Ant matcher harus diupdate jika sudah selesai. Mana yang diperbolehkan sebelum login*/
            .antMatchers("/","/login","/search-guide/**","/register/**","/profile/detail/**","/uploaded/**","/hubungi-kami",
            		"/info/**","/profile/check-password","/profile/check-email","/profile/check-username","/travel-partner/**",
            		"/ajax/**","/components/**","/css/**","/fonts/**","/images/**","/js/**","/data/**")
            	.permitAll()
                .anyRequest().authenticated()
                .and()
            .formLogin()
                .loginPage("/")
                .loginProcessingUrl("/process-login")
                .failureUrl("/?bad-credential")
                .usernameParameter("username")
                .passwordParameter("password")
                .successHandler(successHandler())
                .permitAll()
                .and()
                .rememberMe()
                .rememberMeParameter("rememberMe");
        http
        	.sessionManagement()
        		.sessionCreationPolicy(SessionCreationPolicy.ALWAYS)
        		.invalidSessionUrl("/?exp")
        		.maximumSessions(1);
        
        http
			.logout()
			.logoutUrl("/logout")
				.logoutSuccessUrl("/")
				.invalidateHttpSession(true)
				.deleteCookies("JSESSIONID")
				.deleteCookies("remember-me")
				.permitAll();
        http
        	.csrf()
        	.ignoringAntMatchers("/logout");
    }
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService);
	}
	
	@Bean(name="passwordEncoder")
	public PasswordEncoder passwordencoder(){
		return new BCryptPasswordEncoder();
	}
	
	@Override
	public void onApplicationEvent(AbstractAuthenticationEvent authenticationEvent) {
		if (authenticationEvent instanceof InteractiveAuthenticationSuccessEvent) {
	         // ignores to prevent duplicate logging with AuthenticationSuccessEvent
	         return;
	      }
	      Authentication authentication = authenticationEvent.getAuthentication();
	      String auditMessage = "Login attempt with username: " + authentication.getName() + " Success: " + authentication.isAuthenticated();
	      logger.info("####### [SPRING SECURITY] Auth message: "+auditMessage);
	}
	
	@Bean
	public AuthenticationSuccessHandler successHandler() {
	    SimpleUrlAuthenticationSuccessHandler handler = new SimpleUrlAuthenticationSuccessHandler();
	    handler.setUseReferer(true);
	    return handler;
	}
	
	
}
