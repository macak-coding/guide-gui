package com.guideme.gui.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.messaging.MessageSecurityMetadataSourceRegistry;
import org.springframework.security.config.annotation.web.socket.AbstractSecurityWebSocketMessageBrokerConfigurer;

@Configuration
public class WebSocketSecurityConfig extends AbstractSecurityWebSocketMessageBrokerConfigurer{
	protected void configureInbound(MessageSecurityMetadataSourceRegistry messages) {
        messages
        	.nullDestMatcher().authenticated()
        	.simpDestMatchers("/chat/group/**").authenticated()
        	.simpSubscribeDestMatchers("/topic/group/**").authenticated()
        	.anyMessage().denyAll();
    }
}
