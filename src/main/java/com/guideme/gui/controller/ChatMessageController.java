package com.guideme.gui.controller;

import org.apache.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.guideme.common.entity.ChatMessage;
import com.guideme.common.model.StatusResponse;
import com.guideme.gui.model.PageableWrapper;
import com.guideme.gui.service.inf.ChatMessageServiceInf;

@Controller
@RequestMapping("/chat-message")
public class ChatMessageController {
	private static final Logger logger = Logger.getLogger(ChatMessageController.class);
	
	private ChatMessageServiceInf chatMessageService;
	
	public ChatMessageController(ChatMessageServiceInf chatMessageService){
		this.chatMessageService = chatMessageService;
	}
	
	@RequestMapping(value="/{token}/{page}", method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity getByTokenIdAjax(@PathVariable("token") String token, @PathVariable("page") int page){
		
		StatusResponse statusResponse = chatMessageService.getByToken(token, page);
		
		
		if(statusResponse.getStatus().equalsIgnoreCase("success")){
			PageableWrapper<ChatMessage> chatMessagePage = (PageableWrapper<ChatMessage>) statusResponse.getReturnObject();
			return ResponseEntity.ok(chatMessagePage);
		}else{
			logger.error("[GUI CONTROLLER] "+statusResponse.getMessage());
			logger.error("[GUI CONTROLLER] Dev msg: "+statusResponse.getDevMessage());
			return ResponseEntity.badRequest().build();
		}
	}
}
