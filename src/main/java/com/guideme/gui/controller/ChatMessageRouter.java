package com.guideme.gui.controller;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;

import com.guideme.common.entity.ChatMessage;
import com.guideme.gui.model.MessageModel;
import com.guideme.gui.model.UserDetailCust;
import com.guideme.gui.service.inf.ChatMessageServiceInf;

@Controller
public class ChatMessageRouter {
	private static final Logger logger = Logger.getLogger(ChatMessageRouter.class);
	
	ChatMessageServiceInf chatMessageService;
	
	public ChatMessageRouter(ChatMessageServiceInf chatMessageService){
		this.chatMessageService = chatMessageService;
	}
	
	//@PreAuthorize("hasAnyRole([GUIDE,TRAVELER])")
	@MessageMapping("/group/{id}")
	@SendTo("/topic/group/{id}")
	public MessageModel outgoing(@DestinationVariable String id, MessageModel incomingMessage, @AuthenticationPrincipal UserDetailCust user){
		ChatMessage chatMessage = new ChatMessage();
		chatMessage.setChatToken(id);
		chatMessage.setPengirim(incomingMessage.getSender());
		chatMessage.setPenerima(incomingMessage.getReceiver());
		chatMessage.setPesan(incomingMessage.getMessage());
		chatMessage.setTimestamp(new Date());
		
		chatMessageService.saveChatMessage(chatMessage);
		
		return new MessageModel(incomingMessage.getSender(), incomingMessage.getMessage(), incomingMessage.getReceiver());
	}
	
}
