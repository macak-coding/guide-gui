package com.guideme.gui.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.guideme.gui.model.UserDetailCust;

@Controller
public class CustomErrorController implements ErrorController{
	Logger logger = Logger.getLogger(CustomErrorController.class);
	
	private static final String ERROR_PATH = "/error";
	
	@RequestMapping(value=ERROR_PATH)
	public String errorPage(HttpServletRequest request, Model model, @AuthenticationPrincipal UserDetailCust user){
		logger.error("Error: "+request.getAttribute("javax.servlet.error.message")+", user: "+(null == user ? "anonymous" : user.getUsername()));
		model.addAttribute("errorCode", (Integer) request.getAttribute("javax.servlet.error.status_code"));
		return "error/error-page";
	}

	@Override
	public String getErrorPath() {
		return ERROR_PATH;
	}
}
