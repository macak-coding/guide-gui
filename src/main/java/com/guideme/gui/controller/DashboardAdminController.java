package com.guideme.gui.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.guideme.common.model.StatusResponse;
import com.guideme.gui.model.DtTransaksiWrapper;
import com.guideme.gui.model.DtUserDetailWrapper;
import com.guideme.gui.model.UserDetailCust;
import com.guideme.gui.service.inf.DashboardAdminInf;

@Controller
@RequestMapping("/dashboard-admin")
@PreAuthorize("hasAuthority('ROCKSTAR')")
public class DashboardAdminController {
	private static final Logger logger = Logger.getLogger(DashboardAdminController.class);
	
	DashboardAdminInf dashboardAdminService;
	
	@Autowired
	public DashboardAdminController(DashboardAdminInf dashboardAdminService){
		this.dashboardAdminService = dashboardAdminService;
	}
	
	@RequestMapping(method=RequestMethod.GET)
	String halamanDashboard(@AuthenticationPrincipal UserDetailCust principal, Model model){
		model.addAttribute("principal", principal);
		
		return "dashboard-admin";
	}
	
	@RequestMapping(value="/get-all-user", method=RequestMethod.GET)
	@ResponseBody
	List<DtUserDetailWrapper> listUserDetail(){
		return dashboardAdminService.getAllUser();
	}
	
	@RequestMapping(value="/get-all-transaksi", method=RequestMethod.GET)
	@ResponseBody
	List<DtTransaksiWrapper> getListTransaksi(){
		return dashboardAdminService.getAllTransaksi();
	}
	
	@RequestMapping(value="/suspend-user/{idUser}", method=RequestMethod.PUT)
	@ResponseBody
	ResponseEntity suspendUser(@PathVariable("idUser") int idUser){
		StatusResponse resp = dashboardAdminService.suspendUser(idUser);
		
		if(resp.getStatus().equalsIgnoreCase("success"))
			return ResponseEntity.ok(resp);
		else
			return ResponseEntity.badRequest().body(resp);
		
	}
	
	@RequestMapping(value="/verify-user/{idUser}", method=RequestMethod.PUT)
	@ResponseBody
	ResponseEntity verifyUser(@PathVariable("idUser") int idUser){
		StatusResponse resp = dashboardAdminService.verifyUser(idUser);
		
		if(resp.getStatus().equalsIgnoreCase("success"))
			return ResponseEntity.ok(resp);
		else
			return ResponseEntity.badRequest().body(resp);
		
	}
	
	@RequestMapping(value="/unverify-user/{idUser}", method=RequestMethod.PUT)
	@ResponseBody
	ResponseEntity unverifyUser(@PathVariable("idUser") int idUser){
		StatusResponse resp = dashboardAdminService.unverifyUser(idUser);
		
		if(resp.getStatus().equalsIgnoreCase("success"))
			return ResponseEntity.ok(resp);
		else
			return ResponseEntity.badRequest().body(resp);
		
	}
	
	@RequestMapping(value="/transaksi-approve/{idTransaksi}", method=RequestMethod.PUT)
	@ResponseBody
	ResponseEntity approveTransaksi(@PathVariable("idTransaksi") int idTransaksi){
		StatusResponse resp = dashboardAdminService.approveTransaksi(idTransaksi);
		
		if(resp.getStatus().equalsIgnoreCase("success"))
			return ResponseEntity.ok(resp);
		else
			return ResponseEntity.badRequest().body(resp);
		
	}
	
	@RequestMapping(value="/transaksi-selesai/{idTransaksi}", method=RequestMethod.PUT)
	@ResponseBody
	ResponseEntity selesaiTransaksi(@PathVariable("idTransaksi") int idTransaksi){
		StatusResponse resp = dashboardAdminService.selesaikanTransaksi(idTransaksi);
		
		if(resp.getStatus().equalsIgnoreCase("success"))
			return ResponseEntity.ok(resp);
		else
			return ResponseEntity.badRequest().body(resp);
		
	}
}
