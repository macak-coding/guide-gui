package com.guideme.gui.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import com.guideme.common.entity.Kota;
import com.guideme.common.entity.Notifikasi;
import com.guideme.common.entity.NotifikasiTimeline;
import com.guideme.common.entity.Perjalanan;
import com.guideme.common.entity.User;
import com.guideme.common.entity.UserDetail;
import com.guideme.common.model.CustomPageResponse;
import com.guideme.common.model.StatusResponse;
import com.guideme.gui.model.PageableWrapper;
import com.guideme.gui.model.SearchGuide;
import com.guideme.gui.model.UserDetailCust;
import com.guideme.gui.service.inf.NotifikasiServiceInf;
import com.guideme.gui.service.inf.PerjalananServiceInf;
import com.guideme.gui.util.UserUtil;

@Controller
@RequestMapping(value={"/dashboard"})
public class DashboardController {
Logger logger = Logger.getLogger(UserProfileController.class);
	
	@Value("${core.server.hostname}")
	String CORE_URL;
	
	@Value("${cloudinary.base-url.foto-profil}")
	String URL_FOTO_PROFIL;
	
	@Value("${cloudinary.base-url.foto-verifikasi}")
	String URL_FOTO_VERIVIKASI;
	
	@Value("${cloudinary.base-url-default}")
	String URL_FOTO_DEFAULT;

	private RestTemplate restTemplate;
	private UserUtil userUtil;
	private PerjalananServiceInf perjalananService;
	private NotifikasiServiceInf notifikasiService;
	
	@Autowired
	DashboardController(RestTemplate restTemplate, UserUtil userUtil, 
			PerjalananServiceInf perjalananService, NotifikasiServiceInf notifikasiService){
		this.restTemplate = restTemplate;
		this.userUtil = userUtil;
		this.perjalananService = perjalananService;
		this.notifikasiService = notifikasiService;
	}
	
	@RequestMapping(value={"/",""})
	public String dashboard(){
		return "dashboard1";
	}
	
	@ModelAttribute
	public void profileAttribute(Model model, @AuthenticationPrincipal UserDetailCust user, Pageable pageable){
		logger.debug("####### [GUI CONTROLLER] user active: "+user.getUsername());
		
		String getUserDetailUrl = CORE_URL + "/user/detail/";
		String getAllKotaUrl = CORE_URL + "/kota/get-all/";
		
		ResponseEntity<UserDetail> responUserDetail = null;
		UserDetail userDetail = null;
		
		ResponseEntity<CustomPageResponse> responKota = null;
		List<Kota> listKota = null;
		
		List<Notifikasi> listNotifikasi = null;
		List<NotifikasiTimeline> listNotifikasiTimeline = null;
		
		PageableWrapper<Perjalanan> perjalananPage = null;
		try {
			logger.debug("####### [CONTROLLER GUI] Meminta ke core user detail: "+user.getUsername());
			
			responUserDetail = restTemplate.getForEntity(getUserDetailUrl + user.getUsername()+"/",UserDetail.class);
			userDetail = responUserDetail.getBody();
			
			responKota = restTemplate.getForEntity(getAllKotaUrl,CustomPageResponse.class);
			listKota = responKota.getBody().getContent();
			
			listNotifikasi = notifikasiService.getNotifikasiByIdPenerimaPageable(userDetail.getIdUserDetail(), pageable).getContent();
			
			logger.debug("####### [CONTROLLER GUI] Meminta ke core perjalanan id user: "+userDetail.getIdUserDetail());
			perjalananPage = (PageableWrapper<Perjalanan>) perjalananService.getPerjalananByIdGuideOrTravelerPageable(userDetail.getIdUserDetail(), pageable, user.getUserRoles());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		model.addAttribute("listKota", listKota);
		model.addAttribute("urlFotoProfil", URL_FOTO_PROFIL);
		model.addAttribute("urlFotoVerifikasi", URL_FOTO_VERIVIKASI);
		model.addAttribute("urlFotoDefault", URL_FOTO_DEFAULT);
		model.addAttribute("user",userDetail);
		model.addAttribute("listNotifikasi", listNotifikasi);
		model.addAttribute("perjalananPage", perjalananPage);
	}
}
