package com.guideme.gui.controller;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import com.guideme.common.entity.UserDetail;
import com.guideme.gui.model.SearchGuide;
import com.guideme.gui.model.UserDetailCust;

@Controller
public class HomeController{
	
	@Value("${cloudinary.base-url-default}")
	String URL_FOTO_DEFAULT;
	
	@Value("${core.server.hostname}")
	String CORE_SERVER_ADDRESS;
	
	private RestTemplate restTemplate;
	private MessageSource messageSource;
	
	@Autowired
	public HomeController(RestTemplate restTemplate, MessageSource messageSource){
		this.restTemplate = restTemplate;
		this.messageSource = messageSource;
	}
	
	@RequestMapping(value={"","/"})
	public String home(){
		
		return "homepagex";
	}
	
	@ModelAttribute
	public void populateFormObject(Model model, Locale locale, @AuthenticationPrincipal UserDetailCust user){
		
		/*
		 * Jika user sudah login minta detail user ke core
		 */
		
		ResponseEntity<UserDetail> responUserDetail = null;
		UserDetail userDetail = null;
		
		if(null != user){
			String getUserDetailUrl = CORE_SERVER_ADDRESS + "/user/detail/";
			
			responUserDetail = restTemplate.getForEntity(getUserDetailUrl + user.getUsername(),UserDetail.class);
			userDetail = responUserDetail.getBody();
			
			model.addAttribute("idUser", userDetail.getIdUser().getIdUser());
			model.addAttribute("idUserDetail", userDetail.getIdUserDetail());
		}
		
		model.addAttribute("user", user);
		model.addAttribute("formObject", new SearchGuide());
		model.addAttribute("urlFotoDefault", URL_FOTO_DEFAULT);
		model.addAllAttributes(constructLocales(locale));
	}
	
	private Map<String, String>constructLocales(Locale locale){
		Map<String, String> locales = new HashMap<>();
		locales.put("commonLabelKota", messageSource.getMessage("common.label.kota", null, locale));
		locales.put("commonLabelRating", messageSource.getMessage("common.label.rating", null, locale));
		locales.put("commonLabelJenisKelamin", messageSource.getMessage("common.label.jenis-kelamin", null, locale));
		locales.put("commonLabelUmur", messageSource.getMessage("common.label.umur", null, locale));
		locales.put("commonLabelEmail", messageSource.getMessage("common.label.email", null, locale));
		locales.put("commonLabelTujuan", messageSource.getMessage("common.label.tujuan", null, locale));
		locales.put("commonLabelTglBerangkat", messageSource.getMessage("common.label.tgl-berangkat", null, locale));
		locales.put("commonLabelTglSelesai", messageSource.getMessage("common.label.tgl-selesai", null, locale));
		locales.put("commonLabelQuota", messageSource.getMessage("common.label.quota", null, locale));
		locales.put("commonPlaceholderKota", messageSource.getMessage("common.placeholder.kota", null, locale));
		locales.put("commonPlaceholderEmail", messageSource.getMessage("common.placeholder.email", null, locale));
		locales.put("commonPlaceholderTujuan", messageSource.getMessage("common.placeholder.tujuan", null, locale));
		locales.put("commonPlaceholderQuota", messageSource.getMessage("common.placeholder.quota", null, locale));
		locales.put("commonOptionAll", messageSource.getMessage("common.option.all", null, locale));
		locales.put("commonOptionJenisKelaminPria", messageSource.getMessage("common.option.jenis-kelamin.pria", null, locale));
		locales.put("commonOptionJenisKelaminWanita", messageSource.getMessage("common.option.jenis-kelamin.wanita", null, locale));
		locales.put("commonBtnSearchGuide", messageSource.getMessage("common.btn.search-guide", null, locale));
		locales.put("homeTitle", messageSource.getMessage("home.title", null, locale));
		locales.put("homeTag", messageSource.getMessage("home.tag", null, locale));
		locales.put("homeSgTab", messageSource.getMessage("home.sg.tab", null, locale));
		locales.put("homeSgTitle", messageSource.getMessage("home.sg.title", null, locale));
		locales.put("homeJbTab", messageSource.getMessage("home.jb.tab", null, locale));
		locales.put("homeJbNotice", messageSource.getMessage("home.jb.notice", null, locale));
		locales.put("homeJbMainTitle", messageSource.getMessage("home.jb.main-title", null, locale));
		locales.put("homeJbTitlePostJb", messageSource.getMessage("home.jb.title.post-jb", null, locale));
		locales.put("homeJbTitleCariJb", messageSource.getMessage("home.jb.title.cari-jb", null, locale));
		locales.put("homeJbDescTitle", messageSource.getMessage("home.jb.desc.title", null, locale));
		locales.put("homeJbDescContent1", messageSource.getMessage("home.jb.desc.content1", null, locale));
		locales.put("homeJbDescContent2", messageSource.getMessage("home.jb.desc.content2", null, locale));
		locales.put("homeJbLabelDescPerjalanan", messageSource.getMessage("home.jb.label.desc-perjalanan", null, locale));
		locales.put("homeJbPlaceholderDescPerjalanan", messageSource.getMessage("home.jb.placeholder.desc-perjalanan", null, locale));
		locales.put("homeJbBtnPostPerjalanan", messageSource.getMessage("home.jb.btn.post-perjalanan", null, locale));
		locales.put("homeJbBtnCariTraveler", messageSource.getMessage("home.jb.btn.cari-traveler", null, locale));
		
		locales.put("homePartnersTitle", messageSource.getMessage("home.partners.title", null, locale));
		locales.put("homeGuaranteeVerifLine1", messageSource.getMessage("home.guarantee.verif.line1", null, locale));
		locales.put("homeGuaranteeVerifLine2", messageSource.getMessage("home.guarantee.verif.line2", null, locale));
		locales.put("homeGuaranteeVerifLine3", messageSource.getMessage("home.guarantee.verif.line3", null, locale));
		locales.put("homeGuaranteeProLine1", messageSource.getMessage("home.guarantee.pro.line1", null, locale));
		locales.put("homeGuaranteeProLine2", messageSource.getMessage("home.guarantee.pro.line2", null, locale));
		locales.put("homeGuaranteeProLine3", messageSource.getMessage("home.guarantee.pro.line3", null, locale));
		locales.put("homeGuaranteeStdLine1", messageSource.getMessage("home.guarantee.std.line1", null, locale));
		locales.put("homeGuaranteeStdLine2", messageSource.getMessage("home.guarantee.std.line2", null, locale));
		locales.put("homeGuaranteeStdLine3", messageSource.getMessage("home.guarantee.std.line3", null, locale));
		
		return locales;
	}
	
}
