package com.guideme.gui.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.guideme.common.entity.ChatMessage;
import com.guideme.common.entity.JalanBareng;
import com.guideme.common.entity.JalanBarengAnggota;
import com.guideme.common.model.StatusResponse;
import com.guideme.gui.model.JalanBarengAngotaActionWrapper;
import com.guideme.gui.model.PageableWrapper;
import com.guideme.gui.model.UserDetailCust;
import com.guideme.gui.service.inf.ChatMessageServiceInf;
import com.guideme.gui.service.inf.JalanBarengServiceInf;

@Controller
@RequestMapping("/travel-partner")
public class JalanBarengController {
	private static final Logger logger = Logger.getLogger(JalanBarengController.class);
	
	JalanBarengServiceInf jalanBarengService;
	ChatMessageServiceInf chatMessageService;
	
	@Autowired
	public JalanBarengController(JalanBarengServiceInf jalanBarengServiceInf, ChatMessageServiceInf chatMessageService){
		this.jalanBarengService = jalanBarengServiceInf;
		this.chatMessageService = chatMessageService;
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public String defaultPage(){
		return "jalan-bareng-search";
	}
	
	@RequestMapping(method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity createJalanBareng(@RequestBody JalanBareng jalanBareng){
		logger.info("[CONTROLLER GUI] Request creaat jalan bareng "+jalanBareng.getIdPembuat().getIdUser().getUsername());
		StatusResponse<String> statusResponse = jalanBarengService.createJalanBareng(jalanBareng);
		
		if(statusResponse.getStatus().equalsIgnoreCase("success")){
			return ResponseEntity.ok(statusResponse);
		}else{
			logger.error("[GUI CONTROLLER] Gagal create jalan bareng, id pembuat: "+jalanBareng.getIdPembuat().getIdUserDetail()+" msg: "+statusResponse.getMessage());
			logger.error("[GUI CONTROLLER] Dev msg: "+statusResponse.getDevMessage());
			return ResponseEntity.badRequest().body(statusResponse);
		}
		
	}
	
	@RequestMapping(value="/add-anggota", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity addAnggota(@RequestBody JalanBarengAnggota jalanBarengAnggota){
		
		StatusResponse statusResponse = jalanBarengService.addAnggota(jalanBarengAnggota);
		
		if(statusResponse.getStatus().equalsIgnoreCase("success")){
			return ResponseEntity.ok(statusResponse);
		}else{
			logger.error("[GUI CONTROLLER] Gagal menambah anggota jalan bareng, getIdUserDetail(): "+jalanBarengAnggota.getIdAnggota().getIdUserDetail()+", msg: "+statusResponse.getMessage());
			logger.error("[GUI CONTROLLER] Dev msg: "+statusResponse.getDevMessage());
			return ResponseEntity.badRequest().body(statusResponse);
		}
	}
	
	@RequestMapping("/chat/{id}")
	public ModelAndView messaging(@PathVariable("id") String id, Model model ,@AuthenticationPrincipal UserDetailCust currentUser){
		
		ModelAndView mav = new ModelAndView();
		
		StatusResponse<JalanBareng> statusResponseJalanBareng = jalanBarengService.getByTokenId(id);
		StatusResponse<JalanBareng> statusResponseJalanBarengAnggota = null;
		StatusResponse<PageableWrapper<ChatMessage>> statusResponseChatMessage = null;
		PageableWrapper<ChatMessage> pageChatMessage = null;
		JalanBareng  jalanBareng = null;
		
		if(null != statusResponseJalanBareng.getReturnObject()){
			if(null != currentUser){
				logger.debug("[GUI CONTROLLER] User "+currentUser.getUsername()+" melakukan check jalan bareng dengan token: "+id+" untuk chat");
				
				String statusAnggota = "BARU";
				
				jalanBareng = (JalanBareng) statusResponseJalanBareng.getReturnObject();
				String creator = jalanBareng.getIdPembuat().getIdUser().getUsername();
				
				statusResponseChatMessage = chatMessageService.getByToken(id, 0); // load message awal = 0
				pageChatMessage = (PageableWrapper<ChatMessage>) statusResponseChatMessage.getReturnObject();
				
				statusResponseJalanBarengAnggota = jalanBarengService.getAnggotaByUsername(currentUser.getUsername(), id);
				
				if(statusResponseJalanBarengAnggota.getReturnObject() instanceof JalanBarengAnggota && !creator.equalsIgnoreCase(currentUser.getUsername())){
					JalanBarengAnggota jalanBarengAnggota = (JalanBarengAnggota) statusResponseJalanBarengAnggota.getReturnObject();
					statusAnggota = jalanBarengAnggota.getStatus();
				}
				
				logger.info("[GUI CONTROLLER] Status anggota: "+statusAnggota);
				
				mav.addObject("creator", creator);
				mav.addObject("creatorNamaLengkap", jalanBareng.getIdPembuat().getNamaLengkap());
				mav.addObject("tujuan", jalanBareng.getTujuanPerjalanan());
				mav.addObject("tanggalBerangkat", jalanBareng.getTanggalBerangkat());
				mav.addObject("tanggalSelesai", jalanBareng.getTanggalSelesai());
				mav.addObject("listAnggota", jalanBareng.getJalanBarengAnggotas());
				mav.addObject("statusAnggota", statusAnggota);
				mav.addObject("currentUsername", currentUser.getUsername());
				mav.addObject("idJalanBareng", jalanBareng.getIdJalanBareng());
				mav.addObject("chatMessagePage", pageChatMessage);
				mav.addObject("token", id);
				mav.addObject("statusPerjalanan", jalanBareng.getStatusPerjalanan());
				
			}
			
			mav.setViewName("fragments/jalan-bareng/jalan-bareng-page");
		}else{
			logger.info("[GUI CONTROLLER] Jalan bareng dengan token: "+id+" tidak ditemukan");
			mav.addObject("errorCode", "404");
			mav.setViewName("error/error-page");
		}
		
		return mav;
	}
	
	@RequestMapping(value="/accept-anggota", method=RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity terimaAnggota(@RequestBody JalanBarengAngotaActionWrapper anggota){
		
		StatusResponse statusResponse = jalanBarengService.acceptAnggota(anggota.getIdAnggota(), anggota.getIdJalanBareng());
		
		if(statusResponse.getStatus().equalsIgnoreCase("success")){
			return ResponseEntity.ok(statusResponse);
		}else{
			logger.error("[GUI CONTROLLER] Gagal menerima anggota jalan bareng, idAnggota: "+anggota.getIdAnggota()+", msg: "+statusResponse.getMessage());
			logger.error("[GUI CONTROLLER] Dev msg: "+statusResponse.getDevMessage());
			return ResponseEntity.badRequest().body(statusResponse);
		}
	}
	
	@RequestMapping(value="/reject-anggota", method=RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity rejectAnggota(@RequestBody JalanBarengAngotaActionWrapper anggota){
		
		StatusResponse statusResponse = jalanBarengService.rejectAnggota(anggota.getIdAnggota(), anggota.getIdJalanBareng());
		
		if(statusResponse.getStatus().equalsIgnoreCase("success")){
			return ResponseEntity.ok(statusResponse);
		}else{
			logger.error("[GUI CONTROLLER] Gagal kick anggota jalan bareng, idAnggota: "+anggota.getIdAnggota()+", msg: "+statusResponse.getMessage());
			logger.error("[GUI CONTROLLER] Dev msg: "+statusResponse.getDevMessage());
			return ResponseEntity.badRequest().body(statusResponse);
		}
	}
	
	@RequestMapping(value="/kick-anggota", method=RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity kickAnggota(@RequestBody JalanBarengAngotaActionWrapper anggota){
		
		StatusResponse statusResponse = jalanBarengService.kickAnggota(anggota.getIdAnggota(), anggota.getIdJalanBareng());
		
		if(statusResponse.getStatus().equalsIgnoreCase("success")){
			return ResponseEntity.ok(statusResponse);
		}else{
			logger.error("[GUI CONTROLLER] Gagal reject anggota jalan bareng, idAnggota: "+anggota.getIdAnggota()+", msg: "+statusResponse.getMessage());
			logger.error("[GUI CONTROLLER] Dev msg: "+statusResponse.getDevMessage());
			return ResponseEntity.badRequest().body(statusResponse);
		}
	}
	
	@RequestMapping(value="/search-by-kota/{kotaTujuan}", method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity searchByKota(@PathVariable("kotaTujuan") String kotaTujuan, Pageable pageable){
		
		StatusResponse statusResponse = jalanBarengService.getByKotaTujuan(kotaTujuan, pageable);
		
		if(statusResponse.getStatus().equalsIgnoreCase("success")){
			return ResponseEntity.ok(statusResponse);
		}else{
			logger.error("[GUI CONTROLLER] Gagal mendapatkan event jalan bareng, kota: "+kotaTujuan+", msg: "+statusResponse.getMessage());
			logger.error("[GUI CONTROLLER] Dev msg: "+statusResponse.getDevMessage());
			return ResponseEntity.badRequest().body(statusResponse);
		}
	}
	
	@RequestMapping(value="/search-all", method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity searchAll(Pageable pageable){
		
		StatusResponse statusResponse = jalanBarengService.getAllPageable(pageable);
		
		if(statusResponse.getStatus().equalsIgnoreCase("success")){
			return ResponseEntity.ok(statusResponse);
		}else{
			logger.error("[GUI CONTROLLER] Gagal mendapatkan event jalan bareng all, msg: "+statusResponse.getMessage());
			logger.error("[GUI CONTROLLER] Dev msg: "+statusResponse.getDevMessage());
			return ResponseEntity.badRequest().build();
		}
	}
	
	@RequestMapping(value="/selesai", method=RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity selesaiJalanBareng(@RequestParam int idJalanBareng){
		
		StatusResponse statusResponse = jalanBarengService.selesaikanJalanBareng(idJalanBareng);
		
		if(statusResponse.getStatus().equalsIgnoreCase("success")){
			return ResponseEntity.ok(statusResponse);
		}else{
			logger.error("[GUI CONTROLLER] Gagal menyelesaikan jalan bareng, id: "+idJalanBareng+", msg: "+statusResponse.getMessage());
			logger.error("[GUI CONTROLLER] Dev msg: "+statusResponse.getDevMessage());
			return ResponseEntity.badRequest().body(statusResponse);
		}
	}
}
