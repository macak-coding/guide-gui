package com.guideme.gui.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.guideme.common.entity.Notifikasi;
import com.guideme.common.entity.UserDetail;
import com.guideme.common.model.StatusResponse;
import com.guideme.gui.model.NotifikasiWrapper;
import com.guideme.gui.model.PageableWrapper;
import com.guideme.gui.model.UserDetailCust;
import com.guideme.gui.service.inf.NotifikasiServiceInf;

@Controller
@RequestMapping("/notifikasi")
public class NotifikasiController {
	Logger logger = Logger.getLogger(NotifikasiController.class);
	
	NotifikasiServiceInf notifikasiService;
	
	@Autowired
	public NotifikasiController(NotifikasiServiceInf notifikasiService) {
		this.notifikasiService = notifikasiService;
	}
	
	@RequestMapping(value="/{idNotifikasi}/{idPenerima}", method = RequestMethod.GET)
	@ResponseBody
	public NotifikasiWrapper getNotifikasiByIdNotifikasiAndIdPenerima(@PathVariable("idNotifikasi") int idNotifikasi, @PathVariable("idPenerima") int idPenerima){
		
		NotifikasiWrapper notifikasiWrapper = notifikasiService.getNotifikasi(idNotifikasi, idPenerima);
		
		return notifikasiWrapper;
	}
	
	@RequestMapping(value="/get-pageable/penerima/{idPenerima}", method = RequestMethod.GET)
	@ResponseBody
	public PageableWrapper<Notifikasi> getNotifikasiByIdPenerimaPageable(@PathVariable("idPenerima") int idPenerima, Pageable pageable){
		
		PageableWrapper pageableWrapper = notifikasiService.getNotifikasiByIdPenerimaPageable(idPenerima, pageable);
		
		return pageableWrapper;
	}
	
	@RequestMapping(value="/get-pageable/perjalanan/{idPerjalanan}", method = RequestMethod.GET)
	@ResponseBody
	public String getNotifikasiByIdPerjalananPageable(@PathVariable("idPerjalanan") int idPerjalanan, @AuthenticationPrincipal UserDetailCust user){
		
		String html = notifikasiService.getNotifikasiByIdPerjalanan(idPerjalanan, user.getUserRoles());
		return html;
	}
	
}
