package com.guideme.gui.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.guideme.common.entity.Perjalanan;
import com.guideme.common.entity.Transaksi;
import com.guideme.common.model.StatusResponse;
import com.guideme.gui.model.UserDetailCust;
import com.guideme.gui.service.inf.PerjalananServiceInf;
import com.guideme.gui.service.inf.TransaksiServiceInf;

@Controller
@RequestMapping("/pembayaran")
public class PembayaranController {
	private static final Logger logger = Logger.getLogger(PembayaranController.class);
	
	TransaksiServiceInf transaksiService;
	PerjalananServiceInf perjalananInf;
	
	@Autowired
	public PembayaranController(TransaksiServiceInf transaksiService, PerjalananServiceInf perjalananInf){
		this.transaksiService = transaksiService;
		this.perjalananInf = perjalananInf;
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView halamanPembayaran(){
		ModelAndView mav = new ModelAndView("pembayaran");
		
		return mav;
		
	}
	
	@RequestMapping(value="/{kodeTransaksi}", method=RequestMethod.GET)
	public String pembayaranById(@PathVariable("kodeTransaksi") String kodeTransaksi, @AuthenticationPrincipal UserDetailCust currentUser, Model model){
		StatusResponse respTransaksi = transaksiService.getByIdTransaksi(kodeTransaksi, currentUser);
		StatusResponse respPerjalanan = null;
		Transaksi transaksi = null;
		Perjalanan perjlanan = null;
		
		if(respTransaksi.getStatus().equalsIgnoreCase("success")){
			transaksi = (Transaksi) respTransaksi.getReturnObject();
			respPerjalanan = perjalananInf.getPerjalananByIdPerjalanan(transaksi.getIdPerjalanan());
		}
			
		if(respTransaksi.getStatus().equalsIgnoreCase("success") && respPerjalanan.getStatus().equalsIgnoreCase("success")){
			perjlanan = (Perjalanan) respPerjalanan.getReturnObject();
			
			model.addAttribute("transaksi", transaksi);
			model.addAttribute("perjalanan", perjlanan);
		}
		
		return "pembayaran";
	}
	
}
