package com.guideme.gui.controller;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.guideme.common.entity.Notifikasi;
import com.guideme.common.entity.Perjalanan;
import com.guideme.common.model.StatusResponse;
import com.guideme.gui.model.PageableWrapper;
import com.guideme.gui.model.UpdatePerjalananWrapper;
import com.guideme.gui.model.UserDetailCust;
import com.guideme.gui.service.inf.PerjalananServiceInf;

@Controller
@RequestMapping("/perjalanan")
public class PerjalananController {
	Logger logger = Logger.getLogger(PerjalananController.class);
	
	@Value("${core.server.hostname}")
	private String coreUrl;
	
	RestTemplate restTemplate = null;
	PerjalananServiceInf perjalananService;
	
	@Autowired
	public PerjalananController(RestTemplate restTemplate, PerjalananServiceInf perjalananService) {
		this.restTemplate = restTemplate;
		this.perjalananService = perjalananService;
	}
	
	@RequestMapping(value="/update-status-perjalanan", method=RequestMethod.PUT)
	@ResponseBody
	public StatusResponse updateStatusPerjalanan(@RequestBody UpdatePerjalananWrapper updatePerjalananWrapper){
		logger.debug("[CONTROLLER] update status perjalan dengan id: "+updatePerjalananWrapper.getPerjalanan().getIdPerjalanan()+" status: "+updatePerjalananWrapper.getPerjalanan().getStatusPerjalanan());
		
		ParameterizedTypeReference<StatusResponse> param1 = new ParameterizedTypeReference<StatusResponse>(){};
		ResponseEntity<StatusResponse> resp = null;
		StatusResponse statusResponse = null;
		Map<String, Object> parameterBody = new HashMap<>();
		
		parameterBody.put("idPerjalanan", updatePerjalananWrapper.getPerjalanan().getIdPerjalanan());
		parameterBody.put("statusPerjalanan", updatePerjalananWrapper.getPerjalanan().getStatusPerjalanan());
		parameterBody.put("pesan", updatePerjalananWrapper.getPesan());
		
		try {
			resp = restTemplate.exchange(coreUrl+"/perjalanan/updateStatusPerjalanan", HttpMethod.PUT, new HttpEntity(parameterBody), param1);
			statusResponse = resp.getBody();
		} catch (Exception e) {
			logger.error(e.getMessage());
			return new StatusResponse("failed", "Permintaan gagal. Coba beberapa saat lagi.", e.getMessage());
		}
		
		return statusResponse;
	}
	
	@RequestMapping(value="/perjalanan-pageable/{idGuide}", method=RequestMethod.GET)
	@ResponseBody
	public PageableWrapper<Perjalanan> getPerjalananByIdPage(@PathVariable("idGuideOrTraveler") int idGuideOrTraveler, @AuthenticationPrincipal UserDetailCust user, Pageable pageable){
		logger.debug("[CONTROLLER GUI] Meminta perjalanan berdasarkan id guide/traveler: "+idGuideOrTraveler);
		
		return (PageableWrapper<Perjalanan>) perjalananService.getPerjalananByIdGuideOrTravelerPageable(idGuideOrTraveler, pageable, user.getUserRoles());
	}
	
	
}
