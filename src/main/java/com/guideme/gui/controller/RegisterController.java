package com.guideme.gui.controller;

import java.math.BigInteger;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.guideme.common.entity.RatingDetail;
import com.guideme.common.entity.User;
import com.guideme.common.entity.UserDetail;
import com.guideme.common.model.EmailModel;
import com.guideme.common.model.StatusResponse;

@Controller
@RequestMapping("/register")
public class RegisterController {
	private static Logger logger = Logger.getLogger(RegisterController.class);
	
	@Value("${core.server.hostname}")
	private String CORE_URL;
	
	RestTemplate restTemplate;
	
	@Autowired
	RegisterController(RestTemplate restTemplate){
		this.restTemplate = restTemplate;
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<StatusResponse> registerUser(@RequestBody User user) {
		logger.info("Registering user: " + user.getUsername());
		UserDetail defaultUserDetail = new UserDetail();
		RatingDetail defaultRating = new RatingDetail();
		defaultRating.setTotalRating(0);
		defaultRating.setAlternatif(0);
		defaultRating.setKebersihan(0);
		defaultRating.setKesopanan(0);
		defaultRating.setKetepatanWaktu(0);
		defaultRating.setNavigasi(0);
		defaultRating.setPelayanan(0);
		defaultRating.setPenjadwalan(0);
		defaultUserDetail.setSaldo(new BigInteger("0"));
		user.setStatusAkun("INACTIVE");
		user.setStatusVerifikasi("UNVERIFIED");
		user.setStatusProfile("BARU");
		defaultUserDetail.setIdRatingDetail(defaultRating);
		defaultUserDetail.setIdUser(user);
		defaultUserDetail.setUrlIdentitas("verifikasi/default");
		defaultUserDetail.setUrlIdentitas2("verifikasi/default");
		defaultUserDetail.setJenisIdentitas("SIM");
		defaultUserDetail.setJenisIdentitas2("SIM");
		defaultUserDetail.setUrlFoto("fotoProfile/default");
		
		ResponseEntity<StatusResponse> resp = null;
		try {
			resp = restTemplate.postForEntity(CORE_URL + "/user-detail", defaultUserDetail, StatusResponse.class);
			if(resp.getBody().getStatus().equalsIgnoreCase("success")){
				logger.info(resp.getBody().getMessage());
				
				/*
				EmailModel email = new EmailModel();
				email.setTo(user.getEmail());
				email.setSubject("Konfirmasi pendaftaran berhasil ("+user.getUsername()+")");
				email.setTemplateName("pendaftaran.html");
				
				logger.debug("[CONTROLLER GUI ]Sending email to: "+user.getEmail());
				restTemplate.exchange(CORE_URL + "/email/sendHtml", HttpMethod.POST, new HttpEntity(email), String.class);
				*/
				
				return ResponseEntity.ok(resp.getBody());
			}else{
				logger.error(resp.getBody().getMessage());
				return ResponseEntity.badRequest().body(resp.getBody());
			}
				
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resp.getBody());
		}

	}
	
	@RequestMapping(value = "/verify-email/{token}", method = RequestMethod.GET)
	public ModelAndView verifyEmail(@PathVariable("token") String token){
		ModelAndView mav = new ModelAndView("verify-email");
		ParameterizedTypeReference<Map<String, String>> param1 = new ParameterizedTypeReference<Map<String, String>>(){};
		
		ResponseEntity<Map<String, String>> responseEntity = restTemplate.exchange(CORE_URL+"/email/verifikasiEmail/"+token, HttpMethod.GET, null, param1);
		Map<String, String> verifyResult = responseEntity.getBody();
		
		if(verifyResult.get("status").equalsIgnoreCase("success")){
			mav.addObject("statusVerifikasi", "BERHASIL");
			mav.addObject("message", "Email "+verifyResult.get("email")+" berhasil terverifikasi. Silahkan login pada halaman utama. Terimakasih.");
		}else{
			mav.addObject("statusVerifikasi", "GAGAL");
			mav.addObject("message", "Email kamu gagal terverifikasi. Link verifikasi tidak valid.");
		}
		
		
		return mav;
	}
}
