package com.guideme.gui.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import com.guideme.common.entity.Perjalanan;
import com.guideme.common.entity.RatingPerjalanan;
import com.guideme.common.entity.Review;
import com.guideme.common.entity.UserDetail;
import com.guideme.common.model.StatusResponse;
import com.guideme.gui.model.UserDetailCust;

@Controller
@RequestMapping("/request-guide")
public class RequestGuideController {
	Logger logger = Logger.getLogger(RequestGuideController.class);
	
	@Autowired RestTemplate restTemplate;
	
	@Value("${core.server.hostname}")
	private String CORE_URL;
	
	@RequestMapping(value={"","/"},method = RequestMethod.POST)
	public ResponseEntity proceedRequest(@RequestBody Perjalanan perjalanan,
			@AuthenticationPrincipal UserDetailCust user){
		
		ParameterizedTypeReference<StatusResponse<UserDetail>> param1 = new ParameterizedTypeReference<StatusResponse<UserDetail>>(){};
		ResponseEntity<StatusResponse<UserDetail>> respTravelerDetail = null;
		StatusResponse statusResponseTraveler = null;
		UserDetail traveler = null;
		
		ResponseEntity<StatusResponse<UserDetail>> respGuideDetail = null;
		StatusResponse statusResponseGuide = null;
		UserDetail guide = null;
		
		try {
			respTravelerDetail = restTemplate.exchange(CORE_URL+"/user-detail/"+user.getUsername(), HttpMethod.GET, null, param1);
			statusResponseTraveler = respTravelerDetail.getBody();
			traveler = (UserDetail) statusResponseTraveler.getReturnObject();
			
			respGuideDetail = restTemplate.exchange(CORE_URL+"/user-detail/findByIdUserDetail?idUserDetail="+perjalanan.getIdGuide().getIdUserDetail(), HttpMethod.GET, null, param1);
			statusResponseGuide = respGuideDetail.getBody();
			guide = (UserDetail) statusResponseGuide.getReturnObject();
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			return ResponseEntity.badRequest().body(new StatusResponse("failed", "[GUI] Gagal meminta ID User Detail", e.getMessage())); 
		}
		
		Review defaultReview = new Review();
		defaultReview.setStatusReview("BELUM");
		RatingPerjalanan defaultRatingPerjalanan = new RatingPerjalanan();
		
		perjalanan.setIdReview(defaultReview);
		perjalanan.setIdRatingPerjalanan(defaultRatingPerjalanan);
		perjalanan.setIdTraveler(traveler);
		perjalanan.setIdGuide(guide);
		perjalanan.setStatusPerjalanan("BARU");
		
		ResponseEntity<StatusResponse> resp = null;
		StatusResponse statusResponse = null;
		try {
			resp = restTemplate.postForEntity(CORE_URL+"/perjalanan", perjalanan, StatusResponse.class);
			statusResponse = resp.getBody();
			
			if(statusResponse.getStatus().equalsIgnoreCase("success")){
				logger.info(statusResponse.getMessage());
				return resp;
			}else{
				logger.error(statusResponse.getMessage());
				return ResponseEntity.badRequest().body(statusResponse);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			return ResponseEntity.badRequest().body(new StatusResponse("failed", e.getMessage()));
		}
		
	}
	
}
