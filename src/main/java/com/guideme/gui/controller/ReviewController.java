package com.guideme.gui.controller;

import javax.websocket.server.PathParam;

import org.apache.log4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import com.guideme.common.entity.Perjalanan;
import com.guideme.common.entity.Review;
import com.guideme.common.model.ReviewWrapper;
import com.guideme.common.model.StatusResponse;

@Controller
@RequestMapping("/review")
public class ReviewController {
	Logger logger = Logger.getLogger(ReviewController.class);
	
	@Autowired RestTemplate restTemplate;
	
	@Value("${core.server.hostname}")
	private String coreUrl;
	
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<StatusResponse> saveReview(@RequestBody ReviewWrapper reviewWrapper){
		
		ResponseEntity<StatusResponse> resp = null;
		StatusResponse statusResponse = null;
		
		try {
			reviewWrapper.getReview().setStatusReview("SELESAI"); /* Ubah status review menjadi selesai */
			//resp = restTemplate.postForEntity(coreUrl+"/review", reviewWrapper, StatusResponse.class);
			resp = restTemplate.exchange(coreUrl+"/review", HttpMethod.PUT, new HttpEntity(reviewWrapper), StatusResponse.class);
			statusResponse = resp.getBody();
			if(statusResponse.getStatus().equalsIgnoreCase("success"))
				return resp;
			else
				return ResponseEntity.badRequest().body(statusResponse);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return ResponseEntity.badRequest().body(new StatusResponse("failed", "Gagal menyimpan review", e.getMessage()));
		}
	}
	
	@RequestMapping(value="/get-data-review/{idGuide}/{idTraveler}", method = RequestMethod.GET)
	public ResponseEntity<StatusResponse> getDataReview(@PathVariable(value="idGuide") String idGuide,
			@PathVariable(value="idTraveler") String idTraveler){
		
		logger.info("Mengakses data review dengan id guide: "+idGuide+" id traveler: "+idTraveler);
		
		ResponseEntity<StatusResponse> resp = null;
		StatusResponse statusResponse = null;
		
		try {
			resp = restTemplate.getForEntity(coreUrl+"/perjalanan/getPerjalananForReview"+
					"?idGuide="+idGuide+"&idTraveler="+idTraveler+"&statusPerjalanan="+"SELESAI", StatusResponse.class);
			statusResponse = resp.getBody();
			
			if(statusResponse.getStatus().equalsIgnoreCase("success"))
				return resp;
			else
				return ResponseEntity.badRequest().body(statusResponse);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return ResponseEntity.badRequest().body(new StatusResponse("failed", "Gagal mengambil data review", e.getMessage()));
		}
		
	}
}
