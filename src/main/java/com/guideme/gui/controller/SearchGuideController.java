package com.guideme.gui.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.guideme.common.constanta.Constanta;
import com.guideme.common.entity.Kota;
import com.guideme.common.entity.UserDetail;
import com.guideme.common.model.StatusResponse;
import com.guideme.gui.model.PageableWrapper;
import com.guideme.gui.model.SearchGuide;
import com.guideme.gui.service.inf.SearchGuideServiceInf;

@Controller
@RequestMapping("/search-guide")
public class SearchGuideController {
	Logger logger = Logger.getLogger(SearchGuideController.class);
	
	@Value("${core.server.hostname}")
	String CORE_URL;
	
	@Value("${cloudinary.base-url.foto-profil}")
	String FOTO_PROFIL_URL;
	
	RestTemplate restTemplate;
	SearchGuideServiceInf searchGuideService;
	
	@Autowired
	public SearchGuideController(RestTemplate restTemplate, SearchGuideServiceInf searchGuideService){
		this.restTemplate = restTemplate;
		this.searchGuideService = searchGuideService;
	}
	
	@ModelAttribute
	public void modelAttributSearchGuide(Model model){
		model.addAttribute("fotoProfilUrl", FOTO_PROFIL_URL);
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public ModelAndView searchGuidePost(@RequestAttribute("tujuan") String tujuan){
		
		logger.info("[CONTROLLER GUI] Tujuan: "+tujuan);
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("guide-list-view");
		
		return mav;
	}
	
	@RequestMapping(value = {"/",""}, method = RequestMethod.GET)
	public String searchGuideGet(Model model){
		model.addAttribute("page", null);
		return "guide-list-view";
	}
	
	@RequestMapping(value = "/find-by-criteria", method = RequestMethod.POST)
	public ResponseEntity searchGuideByCriteria(@RequestBody SearchGuide searchGuide, Pageable pageable){
		String criteria = searchGuideService.composeCriteria(searchGuide);
		Page<String> pageResult = null;
		
		logger.info("Criteria: "+criteria);
		try {
			pageResult = searchGuideService.getListUserByCriteria(criteria, pageable);
			return ResponseEntity.ok(pageResult);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			return ResponseEntity.badRequest().body(null);
		}
	}
	
	@RequestMapping(value = "/search-guide-form", method = RequestMethod.GET)
	public String searchGuideForm(Model model){
		model.addAttribute("page", null);
		return "guide-list-view";
	}
	
	@RequestMapping(value = "/search-guide-form", method = RequestMethod.POST)
	public ModelAndView searchGuideForm(@ModelAttribute("formObject") SearchGuide searchGuide, BindingResult bindingResult){
		String criteria = searchGuideService.composeCriteria(searchGuide);
		
		ModelAndView mav = new ModelAndView();
		
		logger.info("[CONTROLLER GUI] Criteria: "+criteria);
		try {
			
			PageableWrapper<UserDetail> pageableWrapper = (PageableWrapper<UserDetail>) searchGuideService.getListUserByCriteriaForm(criteria, null);
			mav.setViewName("guide-list-view");
			return mav.addObject("page",pageableWrapper);
		} catch (Exception e) {
			logger.error(e.getMessage());
			mav.setViewName("error/error-page");
			return mav.addObject("errorCode", "500");
		}
	}
	
	@RequestMapping(value = "/get-kota", method = RequestMethod.GET)
	public ResponseEntity searchKotaLike(@RequestParam("query") String namaKota){
		List<Kota> listKota = null;
		
		try {
			listKota = searchGuideService.getKotaLike(namaKota);
			return ResponseEntity.ok(listKota);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			return ResponseEntity.badRequest().body(listKota);
		}
	}
	
}
