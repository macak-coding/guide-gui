package com.guideme.gui.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/search-traveler")
public class SearchTravelerController {
	private static final Logger logger = Logger.getLogger(SearchTravelerController.class);
	
	SearchTravelerController(){
		
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView defaultPage(){
		ModelAndView mav = new ModelAndView("search-traveler");
		
		return mav;
	}
	
	@RequestMapping(value="/form", method=RequestMethod.POST)
	public ModelAndView formHandler(){
		
		return null;
	}
}
