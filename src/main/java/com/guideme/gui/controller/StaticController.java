package com.guideme.gui.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/info")
public class StaticController {
	
	@RequestMapping("/sitemap")
	public String sitemap(){
		return "pages-sitemap";
	}
	
	@RequestMapping("/service")
	public String service(){
		return "pages-services2";
	}
	
	@RequestMapping("/faq")
	public String faq(){
		return "pages-faq2";
	}
	
	@RequestMapping("/policies")
	public String policies(){
		return "pages-policies";
	}
	
	@RequestMapping("/about-us")
	public String aboutUs(){
		return "pages-aboutus2";
	}
	
	@RequestMapping("/contact-us")
	public String contactUsPage(){
		return "pages-contactus2";
	}
	
}
