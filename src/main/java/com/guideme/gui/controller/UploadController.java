package com.guideme.gui.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.guideme.common.entity.Transaksi;
import com.guideme.common.entity.User;
import com.guideme.common.entity.UserDetail;
import com.guideme.common.model.StatusResponse;
import com.guideme.gui.service.inf.UploadFileServiceInf;

@Controller
@RequestMapping("/upload")
public class UploadController {
	Logger logger = Logger.getLogger(UploadController.class);
	
	
	UploadFileServiceInf uploadService;
	
	@Autowired
	UploadController(UploadFileServiceInf uploadService){
		this.uploadService = uploadService;
	}
	
	@RequestMapping(value = "/upload-foto-profil", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<?> uploadProfilePict(@RequestParam("fileFotoProfile") MultipartFile multipartFile, @RequestParam("idUserDetail") int idUserDetail) {
		logger.debug("[GUI CONTROLLER] upload profil, id user detail: "+idUserDetail);
		StatusResponse statusResponse = null;
		UserDetail userDetail = new UserDetail();
		userDetail.setIdUserDetail(idUserDetail);
		if(!multipartFile.isEmpty()){
			logger.info("[CONTROLLER GUI] Uploading filename: "+multipartFile.getOriginalFilename());
			try {
				statusResponse = (StatusResponse) uploadService.uploadFotoProfil(multipartFile, userDetail);
				if(statusResponse.getStatus().equalsIgnoreCase("success"))
					return ResponseEntity.ok(statusResponse);
				else
					return ResponseEntity.badRequest().body(statusResponse);
			} catch (Exception e) {
				logger.error(e.getMessage());
				logger.error("Upload file gagal. Terjadi Exception");
				return ResponseEntity.badRequest().body(new StatusResponse("failed","Upload file gagal. Terjadi Exception."));
			}
		}else{
			logger.debug("[CONTROLLER GUI] Tidak ada update file image yang diupload");
			return ResponseEntity.ok("Tidak ada file upload");
		}
	}
	
	@RequestMapping(value = "/upload-verifikasi", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> uploadVerifikasi(@RequestParam("fileIdentitas") MultipartFile multipartFile, 
			@RequestParam("idUserDetail") int idUserDetail,@RequestParam("idUser") int idUser , 
			@RequestParam("flagVerifikasi") int flag, @RequestParam("jenisIdentitas1") String jenisIdentitas1,
			@RequestParam("jenisIdentitas2") String jenisIdentitas2) {
		
		logger.debug("Jenis identitas: "+jenisIdentitas1+" dan "+jenisIdentitas2);
		
		StatusResponse statusResponse = null;
		UserDetail userDetail = new UserDetail();
		userDetail.setIdUserDetail(idUserDetail);
		userDetail.setJenisIdentitas(jenisIdentitas1);
		userDetail.setJenisIdentitas2(jenisIdentitas2);
		
		User user = new User();
		user.setIdUser(idUser);
		userDetail.setIdUser(user);
		if(!multipartFile.isEmpty()){
			logger.info("[CONTROLLER GUI] Uploading filename: "+multipartFile.getOriginalFilename());
			try {
				statusResponse = (StatusResponse) uploadService.uploadVerifikasi(multipartFile, userDetail, flag);
				if(statusResponse.getStatus().equalsIgnoreCase("success"))
					return ResponseEntity.ok(statusResponse);
				else
					return ResponseEntity.badRequest().body(statusResponse);
			} catch (Exception e) {
				logger.error(e.getMessage());
				logger.error("Upload file gagal. Terjadi Exception");
				return ResponseEntity.badRequest().body(new StatusResponse("failed","Upload file gagal. Terjadi Exception."));
			}
		}else{
			logger.debug("[CONTROLLER GUI] Tidak ada update file image yang diupload");
			return ResponseEntity.badRequest().body("Tidak ada update file image yang diupload");
		}
	}
	
	@RequestMapping(value = "/bukti-pembayaran", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<?> uploadBuktiTransfer(@RequestParam("buktiPembayaran") MultipartFile multipartFile, @RequestParam("idTransaksi") int idTransaksi) {
		logger.debug("[GUI CONTROLLER] upload bukti transaksi, idTransaksi: "+idTransaksi);
		StatusResponse statusResponse = null;
		Transaksi transaksi = new Transaksi();
		transaksi.setIdTransaksi(idTransaksi);
		if(!multipartFile.isEmpty()){
			logger.info("[CONTROLLER GUI] Uploading filename: "+multipartFile.getOriginalFilename());
			try {
				statusResponse = (StatusResponse) uploadService.uploadBuktiPembayaran(multipartFile, idTransaksi);
				if(statusResponse.getStatus().equalsIgnoreCase("success"))
					return ResponseEntity.ok(statusResponse);
				else
					return ResponseEntity.badRequest().body(statusResponse);
			} catch (Exception e) {
				logger.error(e.getMessage());
				logger.error("Upload file gagal. Terjadi Exception, msg: "+e.getMessage());
				return ResponseEntity.badRequest().body(new StatusResponse("failed","Upload file gagal. Terjadi Exception."));
			}
		}else{
			logger.debug("[CONTROLLER GUI] Tidak ada update file image yang diupload");
			return ResponseEntity.ok("Tidak ada file upload");
		}
	}
	
}
