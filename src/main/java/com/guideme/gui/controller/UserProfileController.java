package com.guideme.gui.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.guideme.common.entity.Perjalanan;
import com.guideme.common.entity.User;
import com.guideme.common.entity.UserDetail;
import com.guideme.common.model.StatusResponse;
import com.guideme.gui.exception.UsernameNotExistException;
import com.guideme.gui.model.SearchGuide;
import com.guideme.gui.model.UserDetailCust;
import com.guideme.gui.service.inf.UserServiceInf;
import com.guideme.gui.util.UrlConstant;
import com.guideme.gui.util.UserUtil;

@Controller
@RequestMapping("/profile")
public class UserProfileController {
	Logger logger = Logger.getLogger(UserProfileController.class);
	
	@Value("${core.server.hostname}")
	private String CORE_URL;
	
	@Value("${cloudinary.base-url.foto-profil}")
	String FOTO_PROFIL_URL;
	
	private RestTemplate restTemplate;
	
	private UserServiceInf userService;
	
	@Autowired
	UserProfileController(RestTemplate restTemplate, UserServiceInf userService){
		this.restTemplate = restTemplate;
		this.userService = userService;
	}
	
	@ModelAttribute
	public void modelAttributSearchGuide(Model model){
		model.addAttribute("fotoProfilUrl", FOTO_PROFIL_URL);
	}
	
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity updateUserDetail(@RequestBody UserDetail userDetail) {
		
		logger.info("[GUI CONTROLLER] updating user detail: "+userDetail.getIdUserDetail());
		String saveUserDetailUrl = CORE_URL + "/user-detail";
		
		ParameterizedTypeReference<StatusResponse<UserDetail>> param1 = new ParameterizedTypeReference<StatusResponse<UserDetail>>(){};
		ResponseEntity<StatusResponse<UserDetail>> resp = null;
		StatusResponse<UserDetail> statusResponse = null;
		
		try {
			resp = restTemplate.exchange(saveUserDetailUrl, HttpMethod.PUT, new HttpEntity(userDetail), param1);
			statusResponse = resp.getBody();
			return resp.ok(statusResponse);
		} catch (Exception e) {
			e.printStackTrace();
		return resp.badRequest().body(statusResponse);
		}
	}
	
	@RequestMapping(value = "/update-email", method = RequestMethod.PUT)
	public ResponseEntity<StatusResponse> updateEmail(@RequestBody User user) {
		logger.info("Updating email "+user.getEmail());
		String saveUserDetailUrl = CORE_URL + "/user/update-email";
		ResponseEntity<StatusResponse> resp = null;
		try {
			resp = restTemplate.postForEntity(saveUserDetailUrl, user, StatusResponse.class);
			if(resp.getBody().getStatus().equalsIgnoreCase("success"))
				return ResponseEntity.ok(resp.getBody());
			else
				return ResponseEntity.badRequest().body(resp.getBody());
		} catch (Exception e) {
			e.printStackTrace();
		return ResponseEntity.badRequest().body(new StatusResponse("failed",e.getMessage()));
		}
	}
	
	@RequestMapping(value = "/update-password", method = RequestMethod.PUT)
	public ResponseEntity<StatusResponse> updatePassword(@RequestBody User user) {
		logger.info("[CONTROLLER GUI] Updating user password.");
		String saveUserDetailUrl = CORE_URL + "/user/update-password";
		ResponseEntity<StatusResponse> resp = null;
		try {
			resp = restTemplate.exchange(saveUserDetailUrl, HttpMethod.PUT, new HttpEntity(user), StatusResponse.class);
			if(resp.getBody().getStatus().equalsIgnoreCase("success"))
				return ResponseEntity.ok(resp.getBody());
			else
				return ResponseEntity.badRequest().body(resp.getBody());
		} catch (Exception e) {
			e.printStackTrace();
		return ResponseEntity.badRequest().body(new StatusResponse("failed",e.getMessage()));
		}
	}
	
	@RequestMapping(value = "/update-status-user", method = RequestMethod.PUT)
	public ResponseEntity<StatusResponse> updateUserStatus(@RequestBody UserDetail userDetail) {
		logger.info("Updating status user dengan id "+userDetail.getIdUserDetail());
		String saveUserDetailUrl = CORE_URL + "/user-detail/update-status-user";
		ResponseEntity<StatusResponse> resp = null;
		try {
			
			resp = restTemplate.exchange(saveUserDetailUrl, HttpMethod.PUT, new HttpEntity(userDetail), StatusResponse.class);
			if(resp.getBody().getStatus().equalsIgnoreCase("success"))
				return ResponseEntity.ok(resp.getBody());
			else
				return ResponseEntity.badRequest().body(resp.getBody());
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().body(new StatusResponse("failed", "[GUI] Terjadi error di controller", e.getMessage()));
		}
	}
	
	@RequestMapping(value = "/detail/{username}", method = RequestMethod.GET)
	public ModelAndView profileDetail(@PathVariable(value="username") String username,
			@AuthenticationPrincipal UserDetailCust user){
		
		ModelAndView mav = new ModelAndView();
		ParameterizedTypeReference<StatusResponse<UserDetail>> param1 = new ParameterizedTypeReference<StatusResponse<UserDetail>>(){};
		ParameterizedTypeReference<StatusResponse<Perjalanan>> param2 = new ParameterizedTypeReference<StatusResponse<Perjalanan>>(){};
		ParameterizedTypeReference<StatusResponse<List<Perjalanan>>> param3 = new ParameterizedTypeReference<StatusResponse<List<Perjalanan>>>(){};
		
		ResponseEntity<StatusResponse<UserDetail>> respGuideDetail = null;
		ResponseEntity<StatusResponse<UserDetail>> respTravelerDetail = null;
		ResponseEntity<StatusResponse<Perjalanan>> respPerjalanan = null;
		ResponseEntity<StatusResponse<List<Perjalanan>>> respListPerjalanan = null;
		StatusResponse statusResponseGuide = null;
		StatusResponse statusResponseTraveler = null;
		StatusResponse statusResponsePerjalanan = null;
		StatusResponse statusResponseListPerjalanan = null;
		UserDetail guide = null;
		UserDetail traveler = null;
		Perjalanan perjalanan = null;
		List<Perjalanan> listPerjalanan = null;
		
		try {
			respGuideDetail = restTemplate.exchange(CORE_URL+"/user-detail/"+username, HttpMethod.GET, null, param1);
			statusResponseGuide = respGuideDetail.getBody();
			guide = (UserDetail) statusResponseGuide.getReturnObject();
			if(null == guide)
				throw new UsernameNotExistException();
			
			respListPerjalanan = restTemplate.exchange(CORE_URL+"/perjalanan/getPerjalananById?idGuide="+guide.getIdUserDetail(), HttpMethod.GET, null, param3);
			statusResponseListPerjalanan = respListPerjalanan.getBody();
			listPerjalanan = (List<Perjalanan>) statusResponseListPerjalanan.getReturnObject();
			
			mav.addObject("listPerjalanan", listPerjalanan);
			
			if(null != user){
				respTravelerDetail = restTemplate.exchange(CORE_URL+"/user-detail/"+user.getUsername()+"/", HttpMethod.GET, null, param1);
				statusResponseTraveler = respTravelerDetail.getBody();
				traveler = (UserDetail) statusResponseTraveler.getReturnObject();
				
				logger.debug("Meminta data perjalanan untuk review: guide: "+username+" traveler: "+user.getUsername());
				respPerjalanan = restTemplate.exchange(CORE_URL+"/perjalanan/getPerjalananForReview"+
						"?idGuide="+guide.getIdUserDetail()
						+"&idTraveler="+traveler.getIdUserDetail()
						+"&statusPerjalanan="+"SELESAI", 
						HttpMethod.GET, null, param2);
				
				statusResponsePerjalanan = respPerjalanan.getBody();
				perjalanan = (Perjalanan) statusResponsePerjalanan.getReturnObject();
				
				mav.addObject("perjalanan", perjalanan);
			}
			
			if(null != traveler)
				mav.addObject("userLoginVerif", traveler.getIdUser().getStatusVerifikasi());
				
			mav.setViewName("guide-detail");
			mav.addObject("userLogin", user);
			mav.addObject("statusResponseGuide", statusResponseGuide);
			return mav;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			mav.setViewName("error/error-page");
			mav.addObject("errorMessage", "User tidak ditemukan");
			mav.addObject("errorCode", 404);
			return mav;
		}
	}
	
	@RequestMapping(value = "/check-password", method = RequestMethod.POST)
	@ResponseBody
	public String checkPassword(@RequestParam String username, @RequestParam String passwordLama) {
		logger.debug("Checking password "+username+"|"+passwordLama);
		
		ResponseEntity<StatusResponse<User>> resp = null;
		ParameterizedTypeReference<StatusResponse<User>> param1 = new ParameterizedTypeReference<StatusResponse<User>>(){};
		String checkPasswordUrl = CORE_URL + "/user/checkPassword/"+username+"/"+passwordLama;
		User userCheckedPassword = null;
		try {
			resp = restTemplate.exchange(checkPasswordUrl, HttpMethod.GET, null, param1);
			userCheckedPassword = (User) resp.getBody().getReturnObject();
			if(null != userCheckedPassword)
				return "{\"valid\":true}";
			else
				return "{\"valid\":false}";
		} catch (Exception e) {
			e.printStackTrace();
			return "{\"valid\":null}";
		}
	}
	
	@RequestMapping(value = "/check-email", method = RequestMethod.POST)
	@ResponseBody
	public String checkEmail(@RequestParam String email) {
		User user = new User();
		user.setEmail(email);
		return userService.checkEmail(user);
	}
	
	@RequestMapping(value = "/check-username", method = RequestMethod.POST)
	@ResponseBody
	public String checkUsername(@RequestParam String username) {
		User user = new User();
		user.setUsername(username);
		return userService.checkUsername(user);
	}
	
}
