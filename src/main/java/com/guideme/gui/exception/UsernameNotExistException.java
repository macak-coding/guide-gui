package com.guideme.gui.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class UsernameNotExistException extends Exception{
	
	public UsernameNotExistException(){
		super("Username tidak diemukan");
	}

}
