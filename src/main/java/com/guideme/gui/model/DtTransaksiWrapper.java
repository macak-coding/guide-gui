package com.guideme.gui.model;

import java.math.BigDecimal;
import java.math.BigInteger;

/*
 * Ini adalah class model untuk mapping ke data table 
 * 
 */

public class DtTransaksiWrapper {
	
	int idTransaksi;
	String tujuan;
	String statusPerjalanan;
	String username;
	String status;
	BigDecimal tarif;
	int durasi;
	BigDecimal jumlah;
	String rekeningGuide;
	String url;
	
	public DtTransaksiWrapper(){}
	
	public DtTransaksiWrapper(int idTransaksi, String tujuan, String statusPerjalanan, String username, String status,
			BigDecimal tarif, int durasi, BigDecimal jumlah, String rekeningGuide, String url) {
		super();
		this.idTransaksi = idTransaksi;
		this.tujuan = tujuan;
		this.statusPerjalanan = statusPerjalanan;
		this.username = username;
		this.status = status;
		this.tarif = tarif;
		this.durasi = durasi;
		this.jumlah = jumlah;
		this.rekeningGuide = rekeningGuide;
		this.url = url;
	}

	public int getIdTransaksi() {
		return idTransaksi;
	}
	public void setIdTransaksi(int idTransaksi) {
		this.idTransaksi = idTransaksi;
	}
	public String getTujuan() {
		return tujuan;
	}
	public void setTujuan(String tujuan) {
		this.tujuan = tujuan;
	}
	public String getStatusPerjalanan() {
		return statusPerjalanan;
	}
	public void setStatusPerjalanan(String statusPerjalanan) {
		this.statusPerjalanan = statusPerjalanan;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public BigDecimal getTarif() {
		return tarif;
	}
	public void setTarif(BigDecimal tarif) {
		this.tarif = tarif;
	}
	public int getDurasi() {
		return durasi;
	}
	public void setDurasi(int durasi) {
		this.durasi = durasi;
	}
	public BigDecimal getJumlah() {
		return jumlah;
	}
	public void setJumlah(BigDecimal jumlah) {
		this.jumlah = jumlah;
	}
	public String getRekeningGuide() {
		return rekeningGuide;
	}
	public void setRekeningGuide(String rekeningGuide) {
		this.rekeningGuide = rekeningGuide;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
}
