package com.guideme.gui.model;

/*
 * Ini adalah class model untuk mapping ke data table 
 * 
 */

public class DtUserDetailWrapper {
	
	int idUser;
	String username;
	String role;
	String email;
	String statusProfile;
	String statusVerifikasi;
	String statusAkun;
	String urlFotoVerifikasi;
	String urlVerif1;
	String urlVerif2;
	
	public DtUserDetailWrapper(){}
	
	public DtUserDetailWrapper(int idUser, String username, String role, String email, String statusProfile, String statusVerifikasi,
			String statusAkun, String urlFotoVerifikasi) {
		super();
		this.idUser = idUser;
		this.username = username;
		this.role = role;
		this.email = email;
		this.statusProfile = statusProfile;
		this.statusVerifikasi = statusVerifikasi;
		this.statusAkun = statusAkun;
		this.urlFotoVerifikasi = urlFotoVerifikasi;
	}
	
	

	public DtUserDetailWrapper(int idUser, String username, String role, String email, String statusProfile,
			String statusVerifikasi, String statusAkun, String urlVerif1, String urlVerif2) {
		super();
		this.idUser = idUser;
		this.username = username;
		this.role = role;
		this.email = email;
		this.statusProfile = statusProfile;
		this.statusVerifikasi = statusVerifikasi;
		this.statusAkun = statusAkun;
		this.urlVerif1 = urlVerif1;
		this.urlVerif2 = urlVerif2;
	}

	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getStatusProfile() {
		return statusProfile;
	}
	public void setStatusProfile(String statusProfile) {
		this.statusProfile = statusProfile;
	}
	public String getStatusVerifikasi() {
		return statusVerifikasi;
	}
	public void setStatusVerifikasi(String statusVerifikasi) {
		this.statusVerifikasi = statusVerifikasi;
	}
	public String getStatusAkun() {
		return statusAkun;
	}
	public void setStatusAkun(String statusAkun) {
		this.statusAkun = statusAkun;
	}
	public String getUrlFotoVerifikasi() {
		return urlFotoVerifikasi;
	}
	public void setUrlFotoVerifikasi(String urlFotoVerifikasi) {
		this.urlFotoVerifikasi = urlFotoVerifikasi;
	}

	public String getUrlVerif1() {
		return urlVerif1;
	}

	public void setUrlVerif1(String urlVerif1) {
		this.urlVerif1 = urlVerif1;
	}

	public String getUrlVerif2() {
		return urlVerif2;
	}

	public void setUrlVerif2(String urlVerif2) {
		this.urlVerif2 = urlVerif2;
	}
}
