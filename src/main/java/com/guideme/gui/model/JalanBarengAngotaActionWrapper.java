package com.guideme.gui.model;

public class JalanBarengAngotaActionWrapper {
	int idAnggota;
	int idJalanBareng;
	
	JalanBarengAngotaActionWrapper(){
	}

	public int getIdAnggota() {
		return idAnggota;
	}

	public void setIdAnggota(int idAnggota) {
		this.idAnggota = idAnggota;
	}

	public int getIdJalanBareng() {
		return idJalanBareng;
	}

	public void setIdJalanBareng(int idJalanBareng) {
		this.idJalanBareng = idJalanBareng;
	}
	
}
