package com.guideme.gui.model;

import com.guideme.common.entity.Notifikasi;

public class NotifikasiWrapper {
	Notifikasi notifikasi = null;
	String htmlContent = null;
	
	NotifikasiWrapper(){}
	
	public NotifikasiWrapper(Notifikasi notifikasi, String htmlContent) {
		super();
		this.notifikasi = notifikasi;
		this.htmlContent = htmlContent;
	}



	public Notifikasi getNotifikasi() {
		return notifikasi;
	}
	public void setNotifikasi(Notifikasi notifikasi) {
		this.notifikasi = notifikasi;
	}
	public String getHtmlContent() {
		return htmlContent;
	}
	public void setHtmlContent(String htmlContent) {
		this.htmlContent = htmlContent;
	}
}
