package com.guideme.gui.model;

public class OutgoingMessage {
	private String sender = "";
	private String receiver = "";
	private String outgoingMessage = "";
	
	public OutgoingMessage() {
	}

	public OutgoingMessage(String outgoingMessage) {
		this.outgoingMessage = outgoingMessage;
	}

	public String getOutgoingMessage() {
		return outgoingMessage;
	}

	public void setOutgoingMessage(String outgoingMessage) {
		this.outgoingMessage = outgoingMessage;
	}
	
	public String getSender() {
		return sender;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}
	
}
