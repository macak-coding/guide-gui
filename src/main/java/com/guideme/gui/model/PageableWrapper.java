package com.guideme.gui.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.guideme.common.entity.Perjalanan;

public class PageableWrapper<T> implements Page<T> {
	private int number;
	private int size;
	private int totalPages;
	private int numberOfElements;
	private long totalElements;
	private boolean previousPage;
	private boolean first;
	private boolean nextPage;
	private boolean last;
	private List<T> content;
	private Sort sort;
	
	@Override
	public int getNumber() {
		return this.number;
	}

	@Override
	public int getSize() {
		return this.size;
	}

	@Override
	public int getNumberOfElements() {
		return this.numberOfElements;
	}

	@Override
	public List<T> getContent() {
		return this.content;
	}

	@Override
	public boolean hasContent() {
		return (content.size() > 0);
	}
	
	@JsonIgnore
	@Override
	public Sort getSort() {
		return this.sort;
	}

	@Override
	public boolean isFirst() {
		return this.first;
	}

	@Override
	public boolean isLast() {
		return this.last;
	}

	@Override
	public boolean hasNext() {
		return this.hasNext();
	}

	@Override
	public boolean hasPrevious() {
		return this.hasPrevious();
	}

	@Override
	public Pageable nextPageable() {
		// TODO Auto-generated method stub
		return this.nextPageable();
	}

	@Override
	public Pageable previousPageable() {
		// TODO Auto-generated method stub
		return this.previousPageable();
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return this.iterator();
	}

	@Override
	public int getTotalPages() {
		// TODO Auto-generated method stub
		return this.totalPages;
	}

	@Override
	public long getTotalElements() {
		// TODO Auto-generated method stub
		return this.totalElements;
	}

	@Override
	public <S> Page<S> map(Converter<? super T, ? extends S> converter) {
		// TODO Auto-generated method stub
		return null;
	}

}
