package com.guideme.gui.model;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class SearchGuide {
	private int tujuan;
	private String tanggalBerangkat;
	private String tanggalSelesai;
	private String umur;
	private String jenisKelamin;
	private String rating;
	private BigDecimal tarif;
	
	public int getTujuan() {
		return tujuan;
	}
	public String getTanggalBerangkat() {
		return tanggalBerangkat;
	}
	public String getTanggalSelesai() {
		return tanggalSelesai;
	}
	public String getUmur() {
		return umur;
	}
	public String getJenisKelamin() {
		return jenisKelamin;
	}
	public String getRating() {
		return rating;
	}
	public BigDecimal getTarif() {
		return tarif;
	}
	public void setTujuan(int tujuan) {
		this.tujuan = tujuan;
	}
	public void setTanggalBerangkat(String tanggalBerangkat) {
		this.tanggalBerangkat = tanggalBerangkat;
	}
	public void setTanggalSelesai(String tanggalSelesai) {
		this.tanggalSelesai = tanggalSelesai;
	}
	public void setUmur(String umur) {
		this.umur = umur;
	}
	public void setJenisKelamin(String jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public void setTarif(BigDecimal tarif) {
		this.tarif = tarif;
	}
	@Override
	public String toString() {
		return "SearchGuide [tujuan=" + tujuan + ", tanggalBerangkat=" + tanggalBerangkat + ", tanggalSelesai="
				+ tanggalSelesai + ", umur=" + umur + ", jenisKelamin=" + jenisKelamin + ", rating=" + rating
				+ ", tarif=" + tarif + "]";
	}
	
}
