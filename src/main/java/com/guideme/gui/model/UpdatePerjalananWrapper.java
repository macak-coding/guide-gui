package com.guideme.gui.model;

import com.guideme.common.entity.Notifikasi;
import com.guideme.common.entity.Perjalanan;

public class UpdatePerjalananWrapper {
	private Perjalanan perjalanan;
	private String pesan;
	
	public Perjalanan getPerjalanan() {
		return perjalanan;
	}
	public void setPerjalanan(Perjalanan perjalanan) {
		this.perjalanan = perjalanan;
	}
	public String getPesan() {
		return pesan;
	}
	public void setPesan(String pesan) {
		this.pesan = pesan;
	}
}
