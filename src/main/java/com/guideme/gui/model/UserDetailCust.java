package com.guideme.gui.model;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;

import com.guideme.common.entity.User;


public class UserDetailCust extends User implements UserDetails {
	
	private User authUser;
	
	public UserDetailCust(User user) {
		super(user);
		this.authUser = user;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return AuthorityUtils.commaSeparatedStringToAuthorityList(authUser.getUserRoles());
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return super.getStatusAkun().equalsIgnoreCase("ACTIVE");
	}
	
	@Override
	public String getUsername() {
		return super.getUsername();
	}

	@Override
	public String getPassword() {
		return super.getPassword();
	}
	
	@Override
	public int getIdUser() {
		return super.getIdUser();
	}
	
	@Override
	public String getStatusVerifikasi() {
		return super.getStatusVerifikasi();
	}

}
