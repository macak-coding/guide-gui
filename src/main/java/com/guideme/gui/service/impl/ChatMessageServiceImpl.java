package com.guideme.gui.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.guideme.common.entity.ChatMessage;
import com.guideme.common.entity.Perjalanan;
import com.guideme.common.model.StatusResponse;
import com.guideme.gui.model.PageableWrapper;
import com.guideme.gui.service.inf.ChatMessageServiceInf;

@Service
public class ChatMessageServiceImpl implements ChatMessageServiceInf{
	private static final Logger logger = Logger.getLogger(ChatMessageServiceImpl.class);
	
	@Value("${core.server.hostname}")
	String CORE_SERVER_ADDRESS;
	
	private RestTemplate restTemplate;
	
	private ChatMessageServiceImpl(RestTemplate restTemplate){
		this.restTemplate = restTemplate;
	}
	
	@Override
	public StatusResponse saveChatMessage(ChatMessage chatMessage) {
		StatusResponse statusResp = null;
		ResponseEntity<StatusResponse> resp = null;
		
		try {
			resp  = restTemplate.exchange(CORE_SERVER_ADDRESS+"/chatMessage", HttpMethod.POST, new HttpEntity<ChatMessage>(chatMessage), StatusResponse.class);
			statusResp = resp.getBody();
		} catch (Exception e) {
			logger.error("[GUI SERVICE] Gagal save chat message token: "+chatMessage.getIdChat()+", pengirim: "+chatMessage.getPengirim()+", penerima: "+chatMessage.getPenerima());
			e.printStackTrace();
		}
		
		return statusResp;
	}

	@Override
	public StatusResponse getByPengirim(String pengirim, int page) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StatusResponse getByPenerima(String penerima, int page) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StatusResponse getByToken(String token, int page) {
		ParameterizedTypeReference<StatusResponse<PageableWrapper<ChatMessage>>> param = new ParameterizedTypeReference<StatusResponse<PageableWrapper<ChatMessage>>>(){};
		ResponseEntity<StatusResponse<PageableWrapper<ChatMessage>>> resp = null;
		StatusResponse<PageableWrapper<ChatMessage>> statusResp = null;
		
		String pageParam = "?sort=timestamp,desc&size=20&page="+page;
		
		try {
			resp  = restTemplate.exchange(CORE_SERVER_ADDRESS+"/chatMessage/"+token+pageParam, HttpMethod.GET, null, param);
			statusResp = resp.getBody();
		} catch (Exception e) {
			logger.error("[GUI SERVICE] Gagal save chat message token: "+token);
			e.printStackTrace();
		}
		
		return statusResp;
	}

}
