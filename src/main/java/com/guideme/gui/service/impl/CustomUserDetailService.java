package com.guideme.gui.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.guideme.common.entity.User;
import com.guideme.gui.model.UserDetailCust;

@Service
public class CustomUserDetailService implements UserDetailsService {
	static final Logger logger = LoggerFactory.getLogger(CustomUserDetailService.class);
	
	@Value("${core.server.hostname}")
	private String CORE_SERVER_HOSTNAME;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Override
	public UserDetails loadUserByUsername(String usernameOrEmail) {
		logger.info("[SERVICE AUTH GUI] Authenticating user: " + usernameOrEmail);
		
		//String data[] = usernameOrEmail.split("\\|");
		
		ResponseEntity<User> resp = null;
		User authenticatedUser = null;
		
		String authenticationUrl = CORE_SERVER_HOSTNAME+"/user/" + usernameOrEmail;
		logger.info("[SERVICE AUTH GUI] Authenticating via URL: " + authenticationUrl);
		
		try {
			resp = restTemplate.getForEntity(authenticationUrl, User.class);
			authenticatedUser = resp.getBody();
		} catch (Exception e) {
			throw new AuthenticationException("Can not communicate with authentication serrver. Please try again later.") {
			};
		}
		
		if(null == authenticatedUser){
			throw new AuthenticationException("Username or password mismatch.") {
			};
		}else{
			return new UserDetailCust(authenticatedUser);
		}
	}
}
