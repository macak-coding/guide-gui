package com.guideme.gui.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.guideme.common.entity.Transaksi;
import com.guideme.common.entity.UserDetail;
import com.guideme.common.entity.ViewDtTransaksi;
import com.guideme.common.entity.ViewDtUserDetail;
import com.guideme.common.model.StatusResponse;
import com.guideme.gui.model.DtTransaksiWrapper;
import com.guideme.gui.model.DtUserDetailWrapper;
import com.guideme.gui.service.inf.DashboardAdminInf;

@Service
public class DashboardAdminImpl implements DashboardAdminInf{
	private static final Logger logger = Logger.getLogger(DashboardAdminImpl.class);
	
	@Value("${core.server.hostname}")
	String CORE_SERVER_ADDRESS;
	
	@Value("${cloudinary.base-url-default}")
	String BASE_IMAGE_URL;
	
	RestTemplate restTemplate;
	
	@Autowired
	public DashboardAdminImpl(RestTemplate restTemplate){
		this.restTemplate = restTemplate;
	}
	
	@Override
	public List<DtUserDetailWrapper> getAllUser() {
		ParameterizedTypeReference<StatusResponse<List<ViewDtUserDetail>>> param = new ParameterizedTypeReference<StatusResponse<List<ViewDtUserDetail>>>(){};
		ResponseEntity<StatusResponse<List<ViewDtUserDetail>>> respUserDetail = null;
		StatusResponse statusResponse = null;
		List<ViewDtUserDetail> listDtUserDetail = null;
		List<DtUserDetailWrapper> processedUserDetail = null;
		
		respUserDetail = restTemplate.exchange(CORE_SERVER_ADDRESS+"/user-detail", HttpMethod.GET, null, param);
		statusResponse = respUserDetail.getBody();
		
		if(statusResponse.getStatus().equalsIgnoreCase("success")){
			listDtUserDetail = (List<ViewDtUserDetail>) statusResponse.getReturnObject();
			processedUserDetail = new ArrayList<>();
			
			for(ViewDtUserDetail viewDtUserDetail : listDtUserDetail){
				
				if(!viewDtUserDetail.getRole().equalsIgnoreCase("ROCKSTAR")) // Tampilkan semua user kecuali admin
					/*processedUserDetail.add(new DtUserDetailWrapper(viewDtUserDetail.getIdUser(), 
							viewDtUserDetail.getUsername(), viewDtUserDetail.getRole(), viewDtUserDetail.getEmail(), 
							viewDtUserDetail.getStatusProfile(), viewDtUserDetail.getStatusVerifikasi(), 
							viewDtUserDetail.getStatusAkun(), viewDtUserDetail.getUrlVerif1(), 
							viewDtUserDetail.getUrlVerif2()));*/
				
				processedUserDetail.add(new DtUserDetailWrapper(viewDtUserDetail.getIdUser(), 
						viewDtUserDetail.getUsername(), viewDtUserDetail.getRole(), viewDtUserDetail.getEmail(), 
						viewDtUserDetail.getStatusProfile(), viewDtUserDetail.getStatusVerifikasi(), 
						viewDtUserDetail.getStatusAkun(), constructHtmlUrlVerifikasi(viewDtUserDetail.getUrlVerif1(), 
						viewDtUserDetail.getUrlVerif2())));
			
			}
		}
		
		return processedUserDetail;
	}

	@Override
	public List<DtTransaksiWrapper> getAllTransaksi() {
		ParameterizedTypeReference<StatusResponse<List<ViewDtTransaksi>>> param = new ParameterizedTypeReference<StatusResponse<List<ViewDtTransaksi>>>(){};
		ResponseEntity<StatusResponse<List<ViewDtTransaksi>>> respTransaksi = null;
		StatusResponse statusResponse = null;
		List<ViewDtTransaksi> listDtTransaksi = null;
		List<DtTransaksiWrapper> processedTransaksi = null;
		
		respTransaksi = restTemplate.exchange(CORE_SERVER_ADDRESS+"/transaksi/getForAdmin", HttpMethod.GET, null, param);
		statusResponse = respTransaksi.getBody();
		
		if(statusResponse.getStatus().equalsIgnoreCase("success")){
			listDtTransaksi = (List<ViewDtTransaksi>) statusResponse.getReturnObject();
			processedTransaksi = new ArrayList<>();
			
			for(ViewDtTransaksi transaksi : listDtTransaksi){
				processedTransaksi.add(
						new DtTransaksiWrapper(transaksi.getIdTransaksi(), transaksi.getTujuan(), 
						transaksi.getStatusPerjalanan(), transaksi.getUsername(), 
						transaksi.getStatus(), transaksi.getTarif(), transaksi.getDurasi(), 
						transaksi.getJumlah(), transaksi.getRekGuide(), constructHtmlUrlPembayaran(transaksi.getUrl()))
				);
			
			}
		}
		
		return processedTransaksi;
	}
	
	@Override
	public StatusResponse verifyUser(int idUser) {
		StatusResponse statusResponse = null;
		ResponseEntity<StatusResponse> respUserDetail = restTemplate.exchange(CORE_SERVER_ADDRESS+"/user/verify/"+idUser, HttpMethod.PUT, null, StatusResponse.class);
		statusResponse = respUserDetail.getBody();
		
		return statusResponse;
	}
	
	@Override
	public StatusResponse unverifyUser(int idUser) {
		StatusResponse statusResponse = null;
		ResponseEntity<StatusResponse> respUserDetail = restTemplate.exchange(CORE_SERVER_ADDRESS+"/user/unverify/"+idUser, HttpMethod.PUT, null, StatusResponse.class);
		statusResponse = respUserDetail.getBody();
		
		return statusResponse;
	}

	@Override
	public StatusResponse suspendUser(int idUser) {
		StatusResponse statusResponse = null;
		ResponseEntity<StatusResponse> respUserDetail = restTemplate.exchange(CORE_SERVER_ADDRESS+"/user/suspend/"+idUser, HttpMethod.PUT, null, StatusResponse.class);
		statusResponse = respUserDetail.getBody();
		
		return statusResponse;
	}
	
	@Override
	public StatusResponse approveTransaksi(int idTransaksi) {
		StatusResponse statusResponse = null;
		ResponseEntity<StatusResponse> respUserDetail = restTemplate.exchange(CORE_SERVER_ADDRESS+"/notifikasiTimeline/approvePembayaran/"+idTransaksi, HttpMethod.PUT, null, StatusResponse.class);
		statusResponse = respUserDetail.getBody();
		
		return statusResponse;
	}

	@Override
	public StatusResponse selesaikanTransaksi(int idTransaksi) {
		StatusResponse statusResponse = null;
		ResponseEntity<StatusResponse> respUserDetail = restTemplate.exchange(CORE_SERVER_ADDRESS+"/notifikasiTimeline/selesaikanPembayaran/"+idTransaksi, HttpMethod.PUT, null, StatusResponse.class);
		statusResponse = respUserDetail.getBody();
		
		return statusResponse;
	}
	
	private String constructHtmlUrlVerifikasi(String url1, String url2){
		return "<a href='"+BASE_IMAGE_URL+url1+"' class='button btn-mini green img-magnific' target='_blank'>URL1</a> <a href='"+BASE_IMAGE_URL+url2+"' class='button btn-mini green img-magnific' target='_blank'>URL2</a>";
	}
	
	private String constructHtmlUrlPembayaran(String url){
		return "<a href='"+BASE_IMAGE_URL+url+"' class='button btn-mini orange' target='_blank'>URL</a>";
	}
	 
	private String constructHtmlAction(int idUser){
		return "<a class='button btn-mini dull-blue verify'>VERIFY</a> <a class='button btn-mini red suspend'>SUSPEND</a>";
	}
	
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	        
	@Scheduled(fixedRate=60000*10)
	private void hitCoreServer(){
		
		ParameterizedTypeReference<Map<String, String>> param = new ParameterizedTypeReference<Map<String, String>>(){};
		ResponseEntity<Map<String, String>> respTransaksi = null;
		Map<String, String> result;
		
		try {
			restTemplate.exchange(CORE_SERVER_ADDRESS, HttpMethod.GET, null, param);
		} catch (Exception e) {
			logger.error("[SERVICE GUI] Core DOWN at "+dateFormat.format(new Date()));
		}
	}

}
