package com.guideme.gui.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactoryBean;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.client.RestTemplate;

import com.guideme.common.constanta.Constanta;
import com.guideme.common.entity.JalanBareng;
import com.guideme.common.entity.JalanBarengAnggota;
import com.guideme.common.entity.Perjalanan;
import com.guideme.common.entity.UserDetail;
import com.guideme.common.entity.ViewSearchJalanBareng;
import com.guideme.common.model.StatusResponse;
import com.guideme.gui.model.PageableWrapper;
import com.guideme.gui.service.inf.JalanBarengServiceInf;
import com.guideme.gui.util.UrlConstant;

import freemarker.template.Configuration;
import freemarker.template.Template;

@Service
public class JalanBarengServiceImpl implements JalanBarengServiceInf{
	private static final Logger logger = Logger.getLogger(JalanBarengServiceImpl.class);
	
	@Value("${core.server.hostname}")
	String CORE_SERVER_ADDRESS;
	
	RestTemplate restTemplate;
	FreeMarkerConfigurationFactoryBean freemarkerConfiguration;
	
	@Autowired
	JalanBarengServiceImpl(RestTemplate restTemplate, FreeMarkerConfigurationFactoryBean freemarkerConfiguration){
		this.restTemplate = restTemplate;
		this.freemarkerConfiguration = freemarkerConfiguration;
	}
	
	@Override
	public StatusResponse<String> createJalanBareng(JalanBareng jalanBareng) {
		ParameterizedTypeReference<StatusResponse<String>> param1 = new ParameterizedTypeReference<StatusResponse<String>>(){};

		ResponseEntity<StatusResponse<String>> resp = null;
		StatusResponse statusRespJalanBareng = null;
		
		jalanBareng.setStatusPerjalanan("BARU");
		
		resp = restTemplate.exchange(CORE_SERVER_ADDRESS+"/jalanBareng", HttpMethod.POST, new HttpEntity(jalanBareng), param1);
		statusRespJalanBareng = resp.getBody();
		
		return statusRespJalanBareng;
	}

	@Override
	public StatusResponse<String> addAnggota(JalanBarengAnggota jalanBarengAnggota) {
		ParameterizedTypeReference<StatusResponse<String>> param1 = new ParameterizedTypeReference<StatusResponse<String>>(){};

		ResponseEntity<StatusResponse<String>> resp = null;
		StatusResponse statusRespJalanBarengAnggota = null;
		
		resp = restTemplate.exchange(CORE_SERVER_ADDRESS+"/jalanBareng/anggota", HttpMethod.POST, new HttpEntity(jalanBarengAnggota), param1);
		statusRespJalanBarengAnggota = resp.getBody();
		
		return statusRespJalanBarengAnggota;
	}
	
	@Override
	public StatusResponse<JalanBareng> getAnggotaByUsername(String username, String token) {
		ParameterizedTypeReference<StatusResponse<JalanBarengAnggota>> param1 = new ParameterizedTypeReference<StatusResponse<JalanBarengAnggota>>(){};
		ResponseEntity<StatusResponse<JalanBarengAnggota>> resp = null;
		StatusResponse statusRespJalanBarengAnggota = null;
		
		resp = restTemplate.exchange(CORE_SERVER_ADDRESS+"/jalanBareng/anggota/"+username+"/"+token, HttpMethod.GET, new HttpEntity(null), param1);
		statusRespJalanBarengAnggota = resp.getBody();
		
		return statusRespJalanBarengAnggota;
	}
	
	@Override
	public StatusResponse getByTokenId(String tokenId) {
		ParameterizedTypeReference<StatusResponse<JalanBareng>> param1 = new ParameterizedTypeReference<StatusResponse<JalanBareng>>(){};

		ResponseEntity<StatusResponse<JalanBareng>> resp = null;
		StatusResponse statusRespJalanBareng = null;
		
		resp = restTemplate.exchange(CORE_SERVER_ADDRESS+"/jalanBareng/tokenId/"+tokenId, HttpMethod.GET, new HttpEntity(null), param1);
		statusRespJalanBareng = resp.getBody();
		
		return statusRespJalanBareng;
	}

	@Override
	public StatusResponse acceptAnggota(int idAnggota, int idJalanBareng) {
		ResponseEntity<StatusResponse> resp = null;
		StatusResponse statusRespJalanBareng = null;
		
		resp = restTemplate.exchange(CORE_SERVER_ADDRESS+"/jalanBareng/anggota/accept/"+idJalanBareng+"/"+idAnggota+"/", HttpMethod.PUT, new HttpEntity(null), StatusResponse.class);
		statusRespJalanBareng = resp.getBody();
		
		return statusRespJalanBareng;
	}

	@Override
	public StatusResponse rejectAnggota(int idAnggota, int idJalanBareng) {
		ResponseEntity<StatusResponse> resp = null;
		StatusResponse statusRespJalanBareng = null;
		
		resp = restTemplate.exchange(CORE_SERVER_ADDRESS+"/jalanBareng/anggota/reject/"+idJalanBareng+"/"+idAnggota+"/", HttpMethod.PUT, new HttpEntity(null), StatusResponse.class);
		statusRespJalanBareng = resp.getBody();
		
		return statusRespJalanBareng;
	}

	@Override
	public StatusResponse kickAnggota(int idAnggota, int idJalanBareng) {
		ResponseEntity<StatusResponse> resp = null;
		StatusResponse statusRespJalanBareng = null;
		
		resp = restTemplate.exchange(CORE_SERVER_ADDRESS+"/jalanBareng/anggota/kick/"+idJalanBareng+"/"+idAnggota+"/", HttpMethod.PUT, new HttpEntity(null), StatusResponse.class);
		statusRespJalanBareng = resp.getBody();
		
		return statusRespJalanBareng;
	}

	@Override
	public StatusResponse getAllPageable(Pageable pageable) {
		logger.debug("####### [SERVICE GUI] Search event jalan bareng kota all");
		
		ParameterizedTypeReference<StatusResponse<PageableWrapper<ViewSearchJalanBareng>>> param1 = new ParameterizedTypeReference<StatusResponse<PageableWrapper<ViewSearchJalanBareng>>>(){};
		ResponseEntity<StatusResponse<PageableWrapper<ViewSearchJalanBareng>>> resp = null;
		StatusResponse<PageableWrapper<ViewSearchJalanBareng>> statusResp = null;
		PageableWrapper<ViewSearchJalanBareng> pageWrapper = null;
		
		Configuration config = null;
		Template template;
		Map<String, ViewSearchJalanBareng> templateVariable = new HashMap<>();
		String htmlContent = null;
		List<String> listHtml = new ArrayList<>();
		StatusResponse resultResp = null;
		
		StringBuffer sb = new StringBuffer("?size=3");
		//sb.append(Constanta.DEFAULT_PAGE_ELEMENT_SIZE);
		sb.append("&page=");
		sb.append(pageable.getPageNumber());
		sb.append("&sort=createdOn,");
		sb.append(Constanta.SORT_DESC);
		
		try {
			resp = restTemplate.exchange(CORE_SERVER_ADDRESS+UrlConstant.URL_JALAN_BARENG_GET_ALL_PAGEABLE+sb.toString(), HttpMethod.GET, null, param1);
			statusResp = resp.getBody();
			pageWrapper = (PageableWrapper<ViewSearchJalanBareng>) statusResp.getReturnObject();
			
			if(statusResp.getStatus().equalsIgnoreCase("success")){
				config = freemarkerConfiguration.createConfiguration();
				
				if(pageWrapper.getContent().size() > 0){
					for(ViewSearchJalanBareng viewSearchJalanBareng : pageWrapper.getContent()){
						template = config.getTemplate("fragments/search-jalan-bareng/list-perjalanan-item.html");
						templateVariable.put("jalanBareng",viewSearchJalanBareng);
						htmlContent = FreeMarkerTemplateUtils.processTemplateIntoString(template, templateVariable);
						listHtml.add(htmlContent);
					}
				}else{
					template = config.getTemplate("fragments/search-jalan-bareng/perjalanan-tidak-ditemukan.html");
					htmlContent = FreeMarkerTemplateUtils.processTemplateIntoString(template, templateVariable);
					listHtml.add(htmlContent);
				}
				
				Map<String, Object> resultObject = new HashMap<>();
				resultObject.put("listHtml", listHtml);
				resultObject.put("totalElements", pageWrapper.getTotalElements());
				resultObject.put("totalPages", pageWrapper.getTotalPages());
				
				resultResp = new StatusResponse<>();
				resultResp.setStatus(statusResp.getStatus());
				resultResp.setMessage(statusResp.getMessage());
				resultResp.setDevMessage(statusResp.getDevMessage());
				resultResp.setReturnObject(resultObject);
			}else{
				template = config.getTemplate("fragments/search-guide/perjalanan-tidak-ditemukan.html");
				htmlContent = FreeMarkerTemplateUtils.processTemplateIntoString(template, templateVariable);
				listHtml.add(htmlContent);
				
				resultResp = new StatusResponse<>();
				resultResp.setStatus(statusResp.getStatus());
				resultResp.setMessage(statusResp.getMessage());
				resultResp.setDevMessage(statusResp.getDevMessage());
				resultResp.setReturnObject(listHtml);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
		return resultResp;
	}

	@Override
	public StatusResponse getByKotaTujuan(String kotaTujuan, Pageable pageable) {
		logger.debug("####### [SERVICE GUI] Search event jalan bareng kota: "+kotaTujuan);
		
		ParameterizedTypeReference<StatusResponse<PageableWrapper<ViewSearchJalanBareng>>> param1 = new ParameterizedTypeReference<StatusResponse<PageableWrapper<ViewSearchJalanBareng>>>(){};
		ResponseEntity<StatusResponse<PageableWrapper<ViewSearchJalanBareng>>> resp = null;
		StatusResponse<PageableWrapper<ViewSearchJalanBareng>> statusResp = null;
		PageableWrapper<ViewSearchJalanBareng> pageWrapper = null;
		
		Configuration config = null;
		Template template;
		Map<String, ViewSearchJalanBareng> templateVariable = new HashMap<>();
		String htmlContent = null;
		List<String> listHtml = new ArrayList<>();
		StatusResponse<List<String>> resultResp = null;
		
		StringBuffer sb = new StringBuffer("?size=");
		sb.append(Constanta.DEFAULT_PAGE_ELEMENT_SIZE);
		sb.append("&page=");
		sb.append(pageable.getPageNumber());
		//sb.append("&sort=tanggalPergi,");
		//sb.append(Constanta.SORT_DESC);
		
		try {
			resp = restTemplate.exchange(CORE_SERVER_ADDRESS+UrlConstant.URL_JALAN_BARENG_GET_BY_KOTA_LIKE+kotaTujuan+sb.toString(), HttpMethod.GET, null, param1);
			statusResp = resp.getBody();
			pageWrapper = (PageableWrapper<ViewSearchJalanBareng>) statusResp.getReturnObject();
			
			if(statusResp.getStatus().equalsIgnoreCase("success")){
				config = freemarkerConfiguration.createConfiguration();
				
				if(pageWrapper.getContent().size() > 0){
					for(ViewSearchJalanBareng viewSearchJalanBareng : pageWrapper.getContent()){
						template = config.getTemplate("fragments/search-jalan-bareng/list-perjalanan-item.html");
						templateVariable.put("jalanBareng",viewSearchJalanBareng);
						htmlContent = FreeMarkerTemplateUtils.processTemplateIntoString(template, templateVariable);
						listHtml.add(htmlContent);
					}
				}else{
					template = config.getTemplate("fragments/search-jalan-bareng/perjalanan-tidak-ditemukan.html");
					htmlContent = FreeMarkerTemplateUtils.processTemplateIntoString(template, templateVariable);
					listHtml.add(htmlContent);
				}
				
				resultResp = new StatusResponse<>();
				resultResp.setStatus(statusResp.getStatus());
				resultResp.setMessage(statusResp.getMessage());
				resultResp.setDevMessage(statusResp.getDevMessage());
				resultResp.setReturnObject(listHtml);
			}else{
				template = config.getTemplate("fragments/search-guide/perjalanan-tidak-ditemukan.html");
				htmlContent = FreeMarkerTemplateUtils.processTemplateIntoString(template, templateVariable);
				listHtml.add(htmlContent);
				
				resultResp = new StatusResponse<>();
				resultResp.setStatus(statusResp.getStatus());
				resultResp.setMessage(statusResp.getMessage());
				resultResp.setDevMessage(statusResp.getDevMessage());
				resultResp.setReturnObject(listHtml);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
		return resultResp;
	}

	@Override
	public StatusResponse selesaikanJalanBareng(int idJalanBareng) {
		ResponseEntity<StatusResponse> resp = null;
		StatusResponse statusRespJalanBarengSelesai = null;
		
		JalanBareng jalanBareng = new JalanBareng(idJalanBareng);
		
		resp = restTemplate.exchange(CORE_SERVER_ADDRESS+"/jalanBareng/selesai", HttpMethod.PUT, new HttpEntity(jalanBareng), StatusResponse.class);
		statusRespJalanBarengSelesai = resp.getBody();
		
		return statusRespJalanBarengSelesai;
	}
	
}
