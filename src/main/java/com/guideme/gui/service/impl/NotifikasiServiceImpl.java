package com.guideme.gui.service.impl;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactoryBean;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.client.RestTemplate;

import com.guideme.common.constanta.Constanta;
import com.guideme.common.entity.Notifikasi;
import com.guideme.common.entity.NotifikasiTimeline;
import com.guideme.common.entity.Perjalanan;
import com.guideme.common.entity.Transaksi;
import com.guideme.common.model.StatusResponse;
import com.guideme.gui.controller.NotifikasiController;
import com.guideme.gui.model.NotifikasiWrapper;
import com.guideme.gui.model.PageableWrapper;
import com.guideme.gui.service.inf.NotifikasiServiceInf;
import com.guideme.gui.util.UrlConstant;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

@Service
public class NotifikasiServiceImpl implements NotifikasiServiceInf {
	Logger logger = Logger.getLogger(NotifikasiServiceImpl.class);
	
	@Value("${core.server.hostname}")
	private String CORE_SERVER_HOSTNAME;
	
	RestTemplate restTemplate = null;
	private FreeMarkerConfigurationFactoryBean freemarkerConfiguration;
	
	@Autowired
	public NotifikasiServiceImpl(RestTemplate restTemplate, FreeMarkerConfigurationFactoryBean freemarkerConfiguration) {
		this.restTemplate = restTemplate;
		this.freemarkerConfiguration = freemarkerConfiguration;
	}
	
	@Override
	public NotifikasiWrapper getNotifikasi(int idNotifikasi, int idPenerima) {
		
		ParameterizedTypeReference<StatusResponse<Notifikasi>> param1 = new ParameterizedTypeReference<StatusResponse<Notifikasi>>(){};
		ResponseEntity<StatusResponse<Notifikasi>> respNotifikasi = null;
		StatusResponse statusResponseNotifikasi = null;
		Notifikasi notifikasi = null;
		
		String htmlContent = null;
		
		Configuration config = null;
		try {
			logger.debug("####### [SERVICE GUI] Meminta notifikasi dengan id notifikasi "+idNotifikasi+" dan id penerima: "+idPenerima);
			config = freemarkerConfiguration.createConfiguration();
			
			respNotifikasi = restTemplate.exchange(CORE_SERVER_HOSTNAME+"/notifikasi/getByIdNotifikasiAndIdPenerima/"+idNotifikasi+"/"+idPenerima, HttpMethod.GET, null, param1);
			statusResponseNotifikasi = respNotifikasi.getBody();
			notifikasi = (Notifikasi) statusResponseNotifikasi.getReturnObject();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		
		logger.debug("####### [SERVICE GUI] Mendapatkan notifikasi dengan tipe notifikasi "+notifikasi.getIdNotifikasiTipe().getIdNotifikasiTipe());
		logger.debug("####### [SERVICE GUI] Mendapatkan notifikasi dengan status perjalanan "+notifikasi.getIdPerjalanan().getStatusPerjalanan());
		Template template;
		Map<String, String> param = new HashMap<>();
		try {
			if(notifikasi.getIdNotifikasiTipe().getIdNotifikasiTipe().equalsIgnoreCase("002") &&
			   notifikasi.getIdPerjalanan().getStatusPerjalanan().equalsIgnoreCase("BARU")){
				template = config.getTemplate("fragments/notifikasi/perjalanan-baru.html");
				param.put("idPerjalanan", notifikasi.getIdPerjalanan().getIdPerjalanan()+"");
				param.put("judulNotifikasi", notifikasi.getIdNotifikasiTipe().getJudulNotifikasi());
				param.put("pesan", notifikasi.getPesan());
				param.put("tujuan", notifikasi.getIdPerjalanan().getTujuan());
				param.put("namaLengkap", notifikasi.getIdPerjalanan().getIdTraveler().getNamaLengkap()+"");
				param.put("jenisKelamin", notifikasi.getIdPerjalanan().getIdTraveler().getJenisKelamin()+"");
				Date tglLahir = notifikasi.getIdPerjalanan().getIdTraveler().getTanggalLahir();
				param.put("umur", getAge(tglLahir)+"");
				param.put("budget", notifikasi.getIdPerjalanan().getBudget()+"");
				param.put("jumlahOrang", notifikasi.getIdPerjalanan().getJumlahOrang()+"");
				param.put("kotaAsal", notifikasi.getIdPerjalanan().getIdTraveler().getIdKota().getNamaKota());
				param.put("tanggalBerangkat", getDate(notifikasi.getIdPerjalanan().getTanggalPergi()));
				param.put("tanggalSelesai", getDate(notifikasi.getIdPerjalanan().getTanggalSelesai()));
				htmlContent = FreeMarkerTemplateUtils.processTemplateIntoString(template, param);
			}else if((notifikasi.getIdNotifikasiTipe().getIdNotifikasiTipe().equalsIgnoreCase("003") ||
					notifikasi.getIdPerjalanan().getStatusPerjalanan().equalsIgnoreCase("DITOLAK")) &&
					!notifikasi.getIdNotifikasiTipe().getIdNotifikasiTipe().equalsIgnoreCase("009")){
				
				ParameterizedTypeReference<StatusResponse<List<Notifikasi>>> param2 = new ParameterizedTypeReference<StatusResponse<List<Notifikasi>>>(){};
				ResponseEntity<StatusResponse<List<Notifikasi>>> respListNotif = null;
				StatusResponse statusResponseNotif = null;
				List<Notifikasi> listNotif = null;
				
				respListNotif = restTemplate.exchange(CORE_SERVER_HOSTNAME+"/notifikasi/getByIdPerjalanan/101", HttpMethod.GET, null, param2);
				statusResponseNotif = respListNotif.getBody();
				listNotif = (List<Notifikasi>) statusResponseNotif.getReturnObject();
				
				Map<String, List<Notifikasi>> paramList = new HashMap<>();
				
				template = config.getTemplate("fragments/notifikasi/perjalanan-timeline.html");
				paramList.put("listNotifikasi", listNotif);
				htmlContent = FreeMarkerTemplateUtils.processTemplateIntoString(template, paramList);
				
				/*template = config.getTemplate("fragments/notifikasi/perjalanan-ditolak.html");
				param.put("idPerjalanan", notifikasi.getIdPerjalanan().getIdPerjalanan()+"");
				param.put("judulNotifikasi", notifikasi.getIdNotifikasiTipe().getJudulNotifikasi());
				param.put("namaGuide", notifikasi.getIdPerjalanan().getIdGuide().getNamaLengkap());
				param.put("tujuan", notifikasi.getIdPerjalanan().getTujuan());
				param.put("pesan", notifikasi.getPesan());
				param.put("tanggalBerangkat", getDate(notifikasi.getIdPerjalanan().getTanggalPergi()));
				param.put("tanggalSelesai", getDate(notifikasi.getIdPerjalanan().getTanggalSelesai()));
				htmlContent = FreeMarkerTemplateUtils.processTemplateIntoString(template, param);*/
			}else if((notifikasi.getIdNotifikasiTipe().getIdNotifikasiTipe().equalsIgnoreCase("004") ||
				 notifikasi.getIdPerjalanan().getStatusPerjalanan().equalsIgnoreCase("SELESAI")) &&
				 (!notifikasi.getIdNotifikasiTipe().getIdNotifikasiTipe().equalsIgnoreCase("007")&&
				 !notifikasi.getIdNotifikasiTipe().getIdNotifikasiTipe().equalsIgnoreCase("008"))){
				
				template = config.getTemplate("fragments/notifikasi/perjalanan-selesai.html");
				param.put("idPerjalanan", notifikasi.getIdPerjalanan().getIdPerjalanan()+"");
				param.put("judulNotifikasi", notifikasi.getIdNotifikasiTipe().getJudulNotifikasi());
				param.put("namaGuide", notifikasi.getIdPerjalanan().getIdGuide().getNamaLengkap());
				param.put("namaTraveler", notifikasi.getIdPerjalanan().getIdTraveler().getNamaLengkap());
				param.put("tujuan", notifikasi.getIdPerjalanan().getTujuan());
				param.put("budget", notifikasi.getIdPerjalanan().getBudget()+"");
				param.put("jumlahOrang", notifikasi.getIdPerjalanan().getJumlahOrang()+"");
				param.put("kotaAsal", notifikasi.getIdPengirim().getIdKota().getNamaKota());
				param.put("tanggalBerangkat", getDate(notifikasi.getIdPerjalanan().getTanggalPergi()));
				param.put("tanggalSelesai", getDate(notifikasi.getIdPerjalanan().getTanggalSelesai()));
				htmlContent = FreeMarkerTemplateUtils.processTemplateIntoString(template, param);
				
			}else if(notifikasi.getIdPerjalanan().getStatusPerjalanan().equalsIgnoreCase("BERJALAN") &&
					!notifikasi.getIdNotifikasiTipe().getIdNotifikasiTipe().equalsIgnoreCase("006")){
				template = config.getTemplate("fragments/notifikasi/perjalanan-berjalan.html");
				param.put("idPerjalanan", notifikasi.getIdPerjalanan().getIdPerjalanan()+"");
				param.put("judulNotifikasi", notifikasi.getIdNotifikasiTipe().getJudulNotifikasi());
				param.put("namaGuide", notifikasi.getIdPerjalanan().getIdGuide().getNamaLengkap());
				param.put("tujuan", notifikasi.getIdPerjalanan().getTujuan());
				param.put("pesan", "Tidak ada pesan");
				param.put("tanggalBerangkat", getDate(notifikasi.getIdPerjalanan().getTanggalPergi()));
				param.put("tanggalSelesai", getDate(notifikasi.getIdPerjalanan().getTanggalSelesai()));
				htmlContent = FreeMarkerTemplateUtils.processTemplateIntoString(template, param);
			}else if((notifikasi.getIdNotifikasiTipe().getIdNotifikasiTipe().equalsIgnoreCase("005") ||
					notifikasi.getIdPerjalanan().getStatusPerjalanan().equalsIgnoreCase("DIBATALKAN")) &&
					!notifikasi.getIdNotifikasiTipe().getIdNotifikasiTipe().equalsIgnoreCase("009")){
				template = config.getTemplate("fragments/notifikasi/perjalanan-dibatalkan.html");
				param.put("idPerjalanan", notifikasi.getIdPerjalanan().getIdPerjalanan()+"");
				param.put("judulNotifikasi", notifikasi.getIdNotifikasiTipe().getJudulNotifikasi());
				param.put("namaGuide", notifikasi.getIdPerjalanan().getIdGuide().getNamaLengkap());
				param.put("tujuan", notifikasi.getIdPerjalanan().getTujuan());
				param.put("pesan", notifikasi.getPesan());
				param.put("tanggalBerangkat", getDate(notifikasi.getIdPerjalanan().getTanggalPergi()));
				param.put("tanggalSelesai", getDate(notifikasi.getIdPerjalanan().getTanggalSelesai()));
				htmlContent = FreeMarkerTemplateUtils.processTemplateIntoString(template, param);
			}else if(notifikasi.getIdNotifikasiTipe().getIdNotifikasiTipe().equalsIgnoreCase("006") &&
					 !notifikasi.getIdPerjalanan().getStatusPerjalanan().equalsIgnoreCase("DIBATALKAN")){
				template = config.getTemplate("fragments/notifikasi/perjalanan-diterima.html");
				param.put("idPerjalanan", notifikasi.getIdPerjalanan().getIdPerjalanan()+"");
				param.put("judulNotifikasi", notifikasi.getIdNotifikasiTipe().getJudulNotifikasi());
				param.put("namaGuide", notifikasi.getIdPerjalanan().getIdGuide().getNamaLengkap());
				param.put("tujuan", notifikasi.getIdPerjalanan().getTujuan());
				param.put("tanggalBerangkat", getDate(notifikasi.getIdPerjalanan().getTanggalPergi()));
				param.put("tanggalSelesai", getDate(notifikasi.getIdPerjalanan().getTanggalSelesai()));
				htmlContent = FreeMarkerTemplateUtils.processTemplateIntoString(template, param);
			}else if(notifikasi.getIdNotifikasiTipe().getIdNotifikasiTipe().equalsIgnoreCase("007")){
				template = config.getTemplate("fragments/notifikasi/perjalanan-mendapat-review.html");
				param.put("idPerjalanan", notifikasi.getIdPerjalanan().getIdPerjalanan()+"");
				param.put("judulNotifikasi", notifikasi.getIdNotifikasiTipe().getJudulNotifikasi());
				param.put("tujuan", notifikasi.getIdPerjalanan().getTujuan());
				float rating = notifikasi.getIdPerjalanan().getIdRatingPerjalanan().getTotalRating();
				rating = rating/5*100;
				param.put("rating", rating+"");
				param.put("userReview", notifikasi.getIdPerjalanan().getIdGuide().getIdUser().getUsername());
				param.put("judulReview", notifikasi.getIdPerjalanan().getIdReview().getJudulReview());
				param.put("isiReview", notifikasi.getIdPerjalanan().getIdReview().getIsiReview());
				param.put("kritikSaran", notifikasi.getIdPerjalanan().getIdReview().getKritikSaran());
				htmlContent = FreeMarkerTemplateUtils.processTemplateIntoString(template, param);
			}else if(notifikasi.getIdNotifikasiTipe().getIdNotifikasiTipe().equalsIgnoreCase("008")){
				template = config.getTemplate("fragments/notifikasi/perjalanan-membuat-review.html");
				param.put("idPerjalanan", notifikasi.getIdPerjalanan().getIdPerjalanan()+"");
				param.put("judulNotifikasi", notifikasi.getIdNotifikasiTipe().getJudulNotifikasi());
				param.put("userReview", notifikasi.getIdPerjalanan().getIdGuide().getIdUser().getUsername());
				param.put("namaGuide", notifikasi.getIdPengirim().getNamaLengkap()+"");
				param.put("tujuan", notifikasi.getIdPerjalanan().getTujuan());
				param.put("budget", notifikasi.getIdPerjalanan().getBudget()+"");
				param.put("tanggalBerangkat", getDate(notifikasi.getIdPerjalanan().getTanggalPergi()));
				param.put("tanggalSelesai", getDate(notifikasi.getIdPerjalanan().getTanggalSelesai()));
				htmlContent = FreeMarkerTemplateUtils.processTemplateIntoString(template, param);
			}else if(notifikasi.getIdNotifikasiTipe().getIdNotifikasiTipe().equalsIgnoreCase("009")){
				template = config.getTemplate("fragments/notifikasi/membuat-perjalanan.html");
				param.put("idPerjalanan", notifikasi.getIdPerjalanan().getIdPerjalanan()+"");
				param.put("judulNotifikasi", notifikasi.getIdNotifikasiTipe().getJudulNotifikasi());
				param.put("pesanNotifikasi", notifikasi.getPesan());
				param.put("tujuan", notifikasi.getIdPerjalanan().getTujuan());
				param.put("budget", notifikasi.getIdPerjalanan().getBudget()+"");
				param.put("jumlahOrang", notifikasi.getIdPerjalanan().getJumlahOrang()+"");
				param.put("kotaAsal", notifikasi.getIdPengirim().getIdKota().getNamaKota());
				param.put("tanggalBerangkat", getDate(notifikasi.getIdPerjalanan().getTanggalPergi()));
				param.put("tanggalSelesai", getDate(notifikasi.getIdPerjalanan().getTanggalSelesai()));
				htmlContent = FreeMarkerTemplateUtils.processTemplateIntoString(template, param);
			}
		
		} catch (IOException | TemplateException e) {
			logger.error("[SERVICE GUI] getNotifikasi() "+e.getMessage());
			e.printStackTrace();
		}
		return new NotifikasiWrapper(notifikasi, htmlContent);
	}

	@Override
	public PageableWrapper<Notifikasi> getNotifikasiByIdPenerimaPageable(int idPenerima, Pageable pageable) {
		
		ParameterizedTypeReference<PageableWrapper<Notifikasi>> param1 = new ParameterizedTypeReference<PageableWrapper<Notifikasi>>(){};
		ResponseEntity<PageableWrapper<Notifikasi>> respNotifikasiPageable = null;
		PageableWrapper<Notifikasi> notifikasiPageable = null;
		
		try {
			logger.debug("####### [SERVICE GUI] Meminta notifikasi pageable dengan id penerima "+idPenerima);
			respNotifikasiPageable = restTemplate.exchange(CORE_SERVER_HOSTNAME+UrlConstant.URL_NOTIFIKASI_BY_ID_PENERIMA_PAGEABLE+idPenerima+"?size="+Constanta.DEFAULT_PAGE_ELEMENT_SIZE+"&page="+pageable.getPageNumber()+"&sort=createdOn,"+Constanta.SORT_DESC, HttpMethod.GET, null, param1);
			notifikasiPageable = respNotifikasiPageable.getBody();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
		return notifikasiPageable;
	}
	
	@Override
	public String getNotifikasiByIdPerjalanan(int idPerjalanan, String currentRole) {
		
		ParameterizedTypeReference<StatusResponse<Perjalanan>> param1 = new ParameterizedTypeReference<StatusResponse<Perjalanan>>(){};
		ResponseEntity<StatusResponse<Perjalanan>> respPerjalanan = null;
		StatusResponse<Perjalanan> statusResponse = null;
		Perjalanan perjalanan = null;
		String htmlContent = null;
		
		ParameterizedTypeReference<StatusResponse<List<NotifikasiTimeline>>> param2 = new ParameterizedTypeReference<StatusResponse<List<NotifikasiTimeline>>>(){};
		ResponseEntity<StatusResponse<List<NotifikasiTimeline>>> respNotifikasiTimeline = null;
		StatusResponse<List<NotifikasiTimeline>> statusResponseTimeline = null;
		List<NotifikasiTimeline> listNotifikasiTimeline = null;
		String htmlContentTimeline = null;
		
		ParameterizedTypeReference<StatusResponse<Transaksi>> param3 = new ParameterizedTypeReference<StatusResponse<Transaksi>>(){};
		ResponseEntity<StatusResponse<Transaksi>> respTransaksi = null;
		StatusResponse<Transaksi> statusResponseTransaksi = null;
		Transaksi transaksi = null;
		
		try {
			logger.debug("####### [SERVICE GUI] Meminta notifikasi pageable dengan id perjalanan "+idPerjalanan);
			respPerjalanan = restTemplate.exchange(CORE_SERVER_HOSTNAME+UrlConstant.URL_PERJALANAN_BY_ID_PERJALANAN+idPerjalanan, HttpMethod.GET, null, param1);
			statusResponse = respPerjalanan.getBody();
			perjalanan =  (Perjalanan) statusResponse.getReturnObject();
			
			respNotifikasiTimeline = restTemplate.exchange(CORE_SERVER_HOSTNAME+UrlConstant.URL_NOTIFIKASI_TIMELINE_BY_ID_PERJALANAN+idPerjalanan, HttpMethod.GET, null, param2);
			statusResponseTimeline = respNotifikasiTimeline.getBody();
			listNotifikasiTimeline = (List<NotifikasiTimeline>) statusResponseTimeline.getReturnObject();
			
			respTransaksi = restTemplate.exchange(CORE_SERVER_HOSTNAME+UrlConstant.URL_TRANSAKSI_BY_ID_PERJALANAN+idPerjalanan, HttpMethod.GET, null, param3);
			statusResponseTransaksi = respTransaksi.getBody();
			transaksi = (Transaksi) statusResponseTransaksi.getReturnObject();
			
			Map<String, Object> paramList = new HashMap<>();
			Configuration config = freemarkerConfiguration.createConfiguration();
			Template template = config.getTemplate("fragments/notifikasi/perjalanan-timeline.html");
			paramList.put("perjalanan", perjalanan);
			paramList.put("noTelpGuide", perjalanan.getIdGuide().getNoTelp());
			paramList.put("emailGuide", perjalanan.getIdGuide().getIdUser().getEmail());
			paramList.put("listNotifikasiTimeline", listNotifikasiTimeline);
			paramList.put("currentRole", currentRole);
			
			if(null != transaksi){
				paramList.put("idTransaksi", transaksi.getIdTransaksi());
				paramList.put("statusPembayaran", transaksi.getStatusPembayaran());
				paramList.put("kodeTransaksi", transaksi.getKodeTransaksi());
			}
			
			htmlContent = FreeMarkerTemplateUtils.processTemplateIntoString(template, paramList);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[GUI SERVICE] Gagal mendapatkan list notifikasi berdasarkan id perjalanan getNotifikasiByIdPerjalananPageable() "+e.getMessage());
		}
		
		return htmlContent;
	}
	
	@Override
	public String getNotifTimelineByIdPerjalanan(int idPerjalanan) {
		
		return null;
	}
	
	private int getAge(Date tglLahir){
		final long MILLIS_PER_DAY = 1000 * 60 * 60 * 24;
		final long MILLIS_PER_YEAR = 365 * MILLIS_PER_DAY;
		  
		if (tglLahir == null)
		      return 0;
		    long ageMillis = System.currentTimeMillis() - tglLahir.getTime();
		    return (int) (ageMillis / MILLIS_PER_YEAR);
	}
	
	private String getDate(Date date){
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
		String finalDate = sdf.format(date);
		
		return finalDate;
	}

}
