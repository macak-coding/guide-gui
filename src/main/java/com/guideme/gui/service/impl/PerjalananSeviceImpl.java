package com.guideme.gui.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.guideme.common.constanta.Constanta;
import com.guideme.common.entity.Perjalanan;
import com.guideme.common.entity.Transaksi;
import com.guideme.common.model.StatusResponse;
import com.guideme.gui.model.PageableWrapper;
import com.guideme.gui.service.inf.PerjalananServiceInf;
import com.guideme.gui.util.UrlConstant;

@Service
public class PerjalananSeviceImpl implements PerjalananServiceInf{
	Logger logger = Logger.getLogger(PerjalananSeviceImpl.class);
	
	RestTemplate restTemplate;
	
	@Value("${core.server.hostname}")
	private String CORE_URL;
	
	@Autowired
	PerjalananSeviceImpl(RestTemplate restTemplate){
		this.restTemplate = restTemplate;
	}
	
	@Override
	public PageableWrapper getPerjalananByIdGuideOrTravelerPageable(int idGuideOrTraveler, Pageable pageable, String role) {
		ParameterizedTypeReference<PageableWrapper<Perjalanan>> param1 = new ParameterizedTypeReference<PageableWrapper<Perjalanan>>(){};
		ResponseEntity<PageableWrapper<Perjalanan>> resp = null;
		PageableWrapper<Perjalanan> pageWrapper = null;
		
		logger.debug("####### [SERVICE GUI] Role user: "+role);
		try {
			if(role.equalsIgnoreCase(Constanta.USER_ROLE_GUIDE))
				resp = restTemplate.exchange(CORE_URL+UrlConstant.URL_PERJALANAN_BY_ID_GUIDE+idGuideOrTraveler+"?size="+Constanta.DEFAULT_PAGE_ELEMENT_SIZE+"&page="+pageable.getPageNumber()+"&sort=tanggalPergi,"+Constanta.SORT_DESC, HttpMethod.GET, null, param1);
			else if(role.equalsIgnoreCase(Constanta.USER_ROLE_TRAVELER))
				resp = restTemplate.exchange(CORE_URL+UrlConstant.URL_PERJALANAN_BY_ID_TRAVELER+idGuideOrTraveler+"?size="+Constanta.DEFAULT_PAGE_ELEMENT_SIZE+"&page="+pageable.getPageNumber()+"&sort=tanggalPergi,"+Constanta.SORT_DESC+"&sort=idPerjalanan,asc", HttpMethod.GET, null, param1);
			
			if(null != resp){
				pageWrapper = resp.getBody();
			}else{
				logger.debug("####### [SERVICE GUI] Tidak mendapatkan list perjalanan dari core untuk idUserDetail: "+idGuideOrTraveler);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
		return pageWrapper;
	}

	@Override
	public StatusResponse getPerjalananByIdPerjalanan(int idPerjalanan) {
		logger.debug("[GUI SERVICE] getPerjalananByIdPerjalanan(), idPerjalanan: "+idPerjalanan);
		
		String urlServiceGetPerjalananById = CORE_URL + UrlConstant.URL_PERJALANAN_BY_ID_PERJALANAN;
		ParameterizedTypeReference<StatusResponse<Perjalanan>> param1 = new ParameterizedTypeReference<StatusResponse<Perjalanan>>(){};
		ResponseEntity<StatusResponse<Perjalanan>> resp = null;
		StatusResponse<Perjalanan> statusResponsePerjalanan = null;
		Perjalanan perjalanan = null;
		
		try {
			resp = restTemplate.exchange(urlServiceGetPerjalananById+idPerjalanan, HttpMethod.GET, null, param1);
			statusResponsePerjalanan = resp.getBody();
		} catch (Exception e) {
			e.printStackTrace();
			return statusResponsePerjalanan;
		}
		
		return statusResponsePerjalanan;
	}

}
