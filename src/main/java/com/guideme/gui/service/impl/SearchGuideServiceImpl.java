package com.guideme.gui.service.impl;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactoryBean;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.client.RestTemplate;

import com.guideme.common.constanta.Constanta;
import com.guideme.common.entity.Kota;
import com.guideme.common.entity.Perjalanan;
import com.guideme.common.entity.UserDetail;
import com.guideme.common.model.StatusResponse;
import com.guideme.gui.controller.SearchGuideController;
import com.guideme.gui.model.PageableWrapper;
import com.guideme.gui.model.SearchGuide;
import com.guideme.gui.service.inf.SearchGuideServiceInf;
import com.guideme.gui.util.UrlConstant;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

@Service
public class SearchGuideServiceImpl implements SearchGuideServiceInf{
	Logger logger = Logger.getLogger(SearchGuideServiceImpl.class);
	
	@Value("${core.server.hostname}")
	private String CORE_URL;
	
	@Value("${cloudinary.base-url.foto-profil}")
	private String FOTO_PROFIL_URL;
	
	RestTemplate restTemplate;
	FreeMarkerConfigurationFactoryBean freemarkerConfiguration;
	
	@Autowired
	public SearchGuideServiceImpl(RestTemplate restTemplate, FreeMarkerConfigurationFactoryBean freemarkerConfiguration){
		this.restTemplate = restTemplate;
		this.freemarkerConfiguration = freemarkerConfiguration;
	}
	
	@Override
	public String composeCriteria(SearchGuide searchGuide) {
		StringBuffer sb = new StringBuffer("");
		if(searchGuide.getJenisKelamin().equalsIgnoreCase("semua"))
			sb.append("jenisKelamin:");
		else
			sb.append("jenisKelamin:").append(searchGuide.getJenisKelamin());
		
		if(searchGuide.getRating().equalsIgnoreCase("semua"))
			sb.append(",idRatingDetail.totalRating>0");
		else
			sb.append(",idRatingDetail.totalRating>").append(searchGuide.getRating());
		
		if(searchGuide.getTujuan() == -1 || searchGuide.getTujuan() == 0)
			sb.append(",idKota.idKota:");
		else
			sb.append(",idKota.idKota:").append(searchGuide.getTujuan());
		
		sb.append(",idUser.userRoles:GUIDE,idUser.statusAkun:ACTIVE,idUser.statusVerifikasi:VERIFIED,idUser.statusProfile:LENGKAP");
		
		if(searchGuide.getUmur().split("-").length == 2){
			String splitAge[] = searchGuide.getUmur().split("-");
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
			Date now = new Date();
			Calendar cal = Calendar.getInstance();
			
			int umurAwal = Integer.parseInt(splitAge[0]);
			int umurAkhir = Integer.parseInt(splitAge[1]);
			
			cal.setTime(now);
			cal.add(Calendar.YEAR, -umurAwal);
			Date umur1 = cal.getTime();
			
			cal.setTime(now);
			cal.add(Calendar.YEAR, -umurAkhir);
			Date umur2 = cal.getTime();
			
			sb.append(",tanggalLahir<").append(sdf.format(umur1)).append(",").append("tanggalLahir>").append(sdf.format(umur2));
			logger.info("Criteria umur: "+sdf.format(umur1)+" dan "+sdf.format(umur2));
		}
			
		return sb.toString();
	}

	@Override
	public Page<String> getListUserByCriteria(String criteria, Pageable pageable) {
		ParameterizedTypeReference<PageableWrapper<UserDetail>> param1 = new ParameterizedTypeReference<PageableWrapper<UserDetail>>(){};
		ResponseEntity<PageableWrapper<UserDetail>> resp = null;
		PageableWrapper<UserDetail> pageWrapper = null;
		
		PageImpl<String> resultPage = null;
		
		Configuration config = null;
		Template template;
		Map<String, String> templateVariable = new HashMap<>();
		String htmlContent = null;
		List<String> listHtml = new ArrayList<>();
		
		int pageNumber = 0;
		if(null != pageable)
			pageNumber = pageable.getPageNumber();
		
		try {
			resp = restTemplate.exchange(CORE_URL+UrlConstant.URL_USER_DETAIL_CRITERIA+criteria
					+"&size="+Constanta.DEFAULT_PAGE_ELEMENT_SIZE
					+"&page="+pageNumber, HttpMethod.GET, null, param1);
			
			pageWrapper = resp.getBody();
			
			config = freemarkerConfiguration.createConfiguration();
			
			if(pageWrapper.getContent().size() > 0){
				for(UserDetail userDetail : pageWrapper.getContent()){
					template = config.getTemplate("fragments/search-guide/list-guide-item.html");
					float totalRating = userDetail.getIdRatingDetail().getTotalRating();
					templateVariable.put("totalRating", getTotalRating(totalRating));
					templateVariable.put("ratingPersen", getRatingPersen(totalRating));
					templateVariable.put("urlFoto", userDetail.getUrlFoto());
					templateVariable.put("namaLengkap", userDetail.getNamaLengkap());
					templateVariable.put("namaKota", userDetail.getIdKota().getNamaKota());
					templateVariable.put("deskripsiSingkat", userDetail.getDeskripsiSingkat());
					templateVariable.put("jenisKelamin", userDetail.getJenisKelamin());
					templateVariable.put("umur", getAge(userDetail.getTanggalLahir()));
					templateVariable.put("statusUser", userDetail.getStatusUser());
					templateVariable.put("tarif", String.valueOf(userDetail.getTarifDitampilkan()));
					templateVariable.put("username", userDetail.getIdUser().getUsername());
					templateVariable.put("fotoProfilUrl", FOTO_PROFIL_URL);
					htmlContent = FreeMarkerTemplateUtils.processTemplateIntoString(template, templateVariable);
					listHtml.add(htmlContent);
				}
			}else{
				template = config.getTemplate("fragments/search-guide/guide-tidak-ditemukan.html");
				htmlContent = FreeMarkerTemplateUtils.processTemplateIntoString(template, templateVariable);
				listHtml.add(htmlContent);
			}
			
			resultPage = new PageImpl<>(listHtml);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[SREVICE GUI] Gagal meminta data guide dan construct list html getListUserByCriteria, msg: "+e.getMessage());
		}
		
		return (Page<String>) resultPage;
	}

	@Override
	public List<Kota> getKotaLike(String namaKota) {
		ParameterizedTypeReference<List<Kota>> param1 = new ParameterizedTypeReference<List<Kota>>(){};
		ResponseEntity<List<Kota>> resp = null;
		List<Kota> listKota = null;
		
		try {
			resp = restTemplate.exchange(CORE_URL+UrlConstant.URL_KOTA_GET_KOTA_LIKE+namaKota, HttpMethod.GET, null, param1);
			listKota = resp.getBody();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
		return listKota;
	}

	@Override
	public Page<UserDetail> getListUserByCriteriaForm(String criteria, Pageable pageable) {
		ParameterizedTypeReference<PageableWrapper<UserDetail>> param1 = new ParameterizedTypeReference<PageableWrapper<UserDetail>>(){};
		ResponseEntity<PageableWrapper<UserDetail>> resp = null;
		PageableWrapper<UserDetail> pageWrapper = null;
		int pageNumber = 0;
		if(null != pageable)
			pageNumber = pageable.getPageNumber();
		
		try {
			resp = restTemplate.exchange(CORE_URL+UrlConstant.URL_USER_DETAIL_CRITERIA+criteria
					+"&size="+Constanta.DEFAULT_PAGE_ELEMENT_SIZE
					+"&page="+pageNumber, HttpMethod.GET, null, param1);
			pageWrapper = resp.getBody();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
		return pageWrapper;
	}
	
	private String getTotalRating(float totalRating){
		
		return String.format("%.1f", totalRating);
	}
	
	private String getRatingPersen(float totalRating){
		
		int persenRating = (int) ((totalRating/5)*100);
		
		return String.valueOf(persenRating);
	}
	
	private String getAge(Date tglLahir){
		final long MILLIS_PER_DAY = 1000 * 60 * 60 * 24;
		final long MILLIS_PER_YEAR = 365 * MILLIS_PER_DAY;
		  
		if (tglLahir == null)
		      return "0";
		
		long ageMillis = System.currentTimeMillis() - tglLahir.getTime();
		int umur =  (int) (ageMillis / MILLIS_PER_YEAR);
		
		return String.valueOf(umur);
	}

}
