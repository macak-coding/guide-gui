package com.guideme.gui.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.cloudinary.Cloudinary;
import com.guideme.common.entity.Transaksi;
import com.guideme.common.model.StatusResponse;
import com.guideme.gui.model.UserDetailCust;
import com.guideme.gui.service.inf.TransaksiServiceInf;
import com.guideme.gui.util.UrlConstant;

@Service
public class TransaksiServiceImpl implements TransaksiServiceInf{
	private static final Logger logger = Logger.getLogger(TransaksiServiceImpl.class);
	
	@Value("${core.server.hostname}")
	private String CORE_URL;
	
	RestTemplate restTemplate;
	
	@Autowired
	TransaksiServiceImpl(RestTemplate restTemplate){
		this.restTemplate = restTemplate;
	}
	
	@Override
	public StatusResponse getByIdTransaksi(String kodeTransaksi, UserDetailCust currentUser) {
		logger.debug("[GUI SERVICE] getByIdTransaksi(), id trx: "+kodeTransaksi);
		
		String urlServiceGetTransaksiById = CORE_URL + UrlConstant.URL_TRANSAKSI_BASE_URL;
		ParameterizedTypeReference<StatusResponse<Transaksi>> param1 = new ParameterizedTypeReference<StatusResponse<Transaksi>>(){};
		ResponseEntity<StatusResponse<Transaksi>> resp = null;
		StatusResponse<Transaksi> statusResponseTransaksi = null;
		Transaksi transaksi = null;
		
		try {
			resp = restTemplate.exchange(urlServiceGetTransaksiById+"/"+kodeTransaksi+"/"+currentUser.getUsername(), HttpMethod.GET, null, param1);
			statusResponseTransaksi = resp.getBody();
		} catch (Exception e) {
			e.printStackTrace();
			return statusResponseTransaksi;
		}
		
		return statusResponseTransaksi;
	}

}
