package com.guideme.gui.service.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.guideme.common.entity.Transaksi;
import com.guideme.common.entity.UserDetail;
import com.guideme.common.model.StatusResponse;
import com.guideme.gui.service.inf.UploadFileServiceInf;
import com.guideme.gui.util.UrlConstant;

@Service
public class UploadFileServiceImpl implements UploadFileServiceInf<StatusResponse>{
	Logger logger = Logger.getLogger(UploadFileServiceImpl.class);
	
	@Value("${upload-file.location}")
	String DIRECTORY_LOCATION;
	
	@Value("${core.server.hostname}")
	private String CORE_URL;
	
	Cloudinary cloudinary;
	RestTemplate restTemplate;
	
	@Autowired
	UploadFileServiceImpl(RestTemplate restTemplate, Cloudinary cloudinary){
		this.cloudinary = cloudinary;
		this.restTemplate = restTemplate;
	}
	
	@Override
	public StatusResponse saveFile(MultipartFile multipartFile) {
		BufferedOutputStream stream = null;
		
		try {
			// Get the filename and build the local file path (be sure that the
			// application have write permissions on such directory)
			String filename = multipartFile.getOriginalFilename();
			String filepath = Paths.get(DIRECTORY_LOCATION, filename).toString();

			// Save the file locally
			stream = new BufferedOutputStream(new FileOutputStream(new File(filepath)));
			stream.write(multipartFile.getBytes());
			
			return new StatusResponse("success","Upload file berhasil");
		} catch (Exception e) {
			logger.error(e.getMessage());
			logger.error("Upload file gagal.");
			return new StatusResponse("failed","Upload file gagal");
		}finally {
			try {
				stream.close();
			} catch (IOException e) {
				logger.error("Error closing file stream for upload file.");
			}
		}
	}

	@Override
	public StatusResponse uploadFotoProfil(MultipartFile multipartFile, UserDetail idUserDetail) {
		logger.debug("[SERVICE] Uploading image "+multipartFile.getOriginalFilename()+" to Cloudinary");
		String updateFotoProfilUrl = CORE_URL + UrlConstant.URL_PROFIL_UPDATE_FOTO;
		ResponseEntity<StatusResponse> resp = null;
		
		Map result = null;
		File fileUpload = multipartToFile(multipartFile);
		try {
			result = cloudinary.uploader().upload(fileUpload, ObjectUtils.asMap("folder","fotoProfile"));
			idUserDetail.setUrlFoto(result.get("public_id").toString());
			resp = restTemplate.exchange(updateFotoProfilUrl, HttpMethod.PUT, new HttpEntity(idUserDetail), StatusResponse.class);
		} catch (IOException e) {
			e.printStackTrace();
			return new StatusResponse<>("failed", "Upload foto gagal",e.getMessage());
		}
		
		return new StatusResponse<>("success", "Upload foto berhasil",result);
	}
	
	@Override
	public StatusResponse uploadVerifikasi(MultipartFile multipartFile, UserDetail idUserDetail, int flag) {
		logger.debug("[SERVICE] Uploading image "+multipartFile.getOriginalFilename()+" to Cloudinary");
		String updateFotoIdentitasUrl = CORE_URL + UrlConstant.URL_PROFIL_UPDATE_FOTO_IDENTITAS;
		ResponseEntity<StatusResponse> resp = null;
		
		Map result = null;
		File fileUpload = multipartToFile(multipartFile);
		try {
			result = cloudinary.uploader().upload(fileUpload, ObjectUtils.asMap("folder","verifikasi"));
			logger.debug("[CONTROLLER GUI] "+result.toString());
			
			if(flag == 1)
				idUserDetail.setUrlIdentitas(result.get("public_id").toString());
			else
				idUserDetail.setUrlIdentitas2(result.get("public_id").toString());
			resp = restTemplate.exchange(updateFotoIdentitasUrl, HttpMethod.PUT, new HttpEntity(idUserDetail), StatusResponse.class);
		} catch (IOException e) {
			e.printStackTrace();
			return new StatusResponse<>("failed", "Upload foto gagal",e.getMessage());
		}
		
		return new StatusResponse<>("success", "Upload foto berhasil",result.get("secure_url"));
	}

	
	public File multipartToFile(MultipartFile multipart){
		String filename = multipart.getOriginalFilename();
		String filepath = Paths.get(DIRECTORY_LOCATION, filename).toString();
		
	    File convFile = new File(filepath);
	    try {
			multipart.transferTo(convFile);
		} catch (IllegalStateException | IOException e) {
			e.printStackTrace();
		}
	    
	    return convFile;
	}

	@Override
	public StatusResponse uploadBuktiPembayaran(MultipartFile multipartFile, int idTransaksi) {
		logger.debug("[SERVICE] Uploading bukti transaksi "+multipartFile.getOriginalFilename()+" to Cloudinary");
		String serviceUpdateBuktiPembayaran = CORE_URL + UrlConstant.URL_TRANSAKSI_BUKTI_PEMBAYARAN;
		ResponseEntity<StatusResponse> resp = null;
		
		Map result = null;
		File fileUpload = multipartToFile(multipartFile);
		Transaksi transaksi = new Transaksi();
		transaksi.setIdTransaksi(idTransaksi);
		transaksi.setStatusPembayaran("VERIFIKASI");
		try {
			result = cloudinary.uploader().upload(fileUpload, ObjectUtils.asMap("folder","buktiPembayaran"));
			transaksi.setUrlBuktiPembayaran(result.get("public_id").toString());
			resp = restTemplate.exchange(serviceUpdateBuktiPembayaran, HttpMethod.PUT, new HttpEntity(transaksi), StatusResponse.class);
		} catch (IOException e) {
			e.printStackTrace();
			return new StatusResponse<>("failed", "Upload bukti pembayaran gagal",e.getMessage());
		}
		
		return new StatusResponse<>("success", "Upload bukti pembayaran berhasil",result);
	}


}
