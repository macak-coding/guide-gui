package com.guideme.gui.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.guideme.common.entity.User;
import com.guideme.common.model.StatusResponse;
import com.guideme.gui.service.inf.UserServiceInf;
import com.guideme.gui.util.UrlConstant;

@Service
public class UserServiceImpl implements UserServiceInf{
	Logger logger = Logger.getLogger(UserServiceImpl.class);
	
	@Value("${core.server.hostname}")
	String CORE_URL;
	
	@Autowired private RestTemplate restTemplate;
	
	@Override
	public String checkUsername(User user) {
		logger.debug("Checking username "+user.getUsername());
		
		ResponseEntity<StatusResponse<User>> resp = null;
		ParameterizedTypeReference<StatusResponse<User>> param1 = new ParameterizedTypeReference<StatusResponse<User>>(){};
		String checkPasswordUrl = CORE_URL + UrlConstant.URL_USER_CHECK_USERNAME;
		User userCheckedUsername = null;
		try {
			resp = restTemplate.exchange(checkPasswordUrl, HttpMethod.POST, new HttpEntity(user), param1);
			userCheckedUsername = (User) resp.getBody().getReturnObject();
			if(null != userCheckedUsername)
				return "{\"valid\":false}";
			else
				return "{\"valid\":true}";
		} catch (Exception e) {
			e.printStackTrace();
			return "{\"valid\":null}";
		}
	}

	@Override
	public String checkEmail(User user) {
		logger.debug("Checking  email "+user.getEmail());
		
		ResponseEntity<StatusResponse<User>> resp = null;
		ParameterizedTypeReference<StatusResponse<User>> param1 = new ParameterizedTypeReference<StatusResponse<User>>(){};
		String checkPasswordUrl = CORE_URL + UrlConstant.URL_USER_CHECK_EMAIL;
		User userCheckedEmail = null;
		try {
			resp = restTemplate.exchange(checkPasswordUrl, HttpMethod.POST, new HttpEntity(user), param1);
			userCheckedEmail = (User) resp.getBody().getReturnObject();
			if(null != userCheckedEmail)
				return "{\"valid\":false}";
			else
				return "{\"valid\":true}";
		} catch (Exception e) {
			e.printStackTrace();
			return "{\"valid\":null}";
		}
	}
	
}
