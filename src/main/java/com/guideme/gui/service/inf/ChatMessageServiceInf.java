package com.guideme.gui.service.inf;

import org.springframework.data.domain.Pageable;

import com.guideme.common.entity.ChatMessage;
import com.guideme.common.model.StatusResponse;

public interface ChatMessageServiceInf {
	public StatusResponse saveChatMessage(ChatMessage chatMessage);
	public StatusResponse getByPengirim(String pengirim, int page);
	public StatusResponse getByPenerima(String penerima, int page);
	public StatusResponse getByToken(String token, int page);
}
