package com.guideme.gui.service.inf;

import java.util.List;

import com.guideme.common.entity.Transaksi;
import com.guideme.common.entity.UserDetail;
import com.guideme.common.model.StatusResponse;
import com.guideme.gui.model.DtTransaksiWrapper;
import com.guideme.gui.model.DtUserDetailWrapper;

public interface DashboardAdminInf {
	List<DtUserDetailWrapper> getAllUser();
	List<DtTransaksiWrapper> getAllTransaksi();
	StatusResponse approveTransaksi(int idTransaksi);
	StatusResponse selesaikanTransaksi(int idTransaksi);
	StatusResponse verifyUser(int idUser);
	StatusResponse unverifyUser(int idUser);
	StatusResponse suspendUser(int idUser);
}
