package com.guideme.gui.service.inf;

import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import com.guideme.common.entity.JalanBareng;
import com.guideme.common.entity.JalanBarengAnggota;
import com.guideme.common.model.StatusResponse;

public interface JalanBarengServiceInf {
	StatusResponse createJalanBareng(JalanBareng jalanBareng);
	StatusResponse addAnggota(JalanBarengAnggota jalanBarengAnggota);
	StatusResponse acceptAnggota(int idAnggota, int idJalanBareng);
	StatusResponse rejectAnggota(int idAnggota, int idJalanBareng);
	StatusResponse kickAnggota(int idAnggota, int idJalanBareng);
	StatusResponse selesaikanJalanBareng(int idJalanBareng);
	StatusResponse getByTokenId(String tokenId);
	StatusResponse<JalanBareng> getAnggotaByUsername(String username, String token);
	StatusResponse getAllPageable(Pageable pageable);
	StatusResponse getByKotaTujuan(String kotaTujuan, Pageable pageable);
}
