package com.guideme.gui.service.inf;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.guideme.common.entity.Notifikasi;
import com.guideme.common.entity.NotifikasiTimeline;
import com.guideme.gui.model.NotifikasiWrapper;
import com.guideme.gui.model.PageableWrapper;

public interface NotifikasiServiceInf {
	NotifikasiWrapper getNotifikasi(int idNotifikasi, int idPenerima);
	PageableWrapper<Notifikasi> getNotifikasiByIdPenerimaPageable(int idPenerima, Pageable pageable);
	String getNotifikasiByIdPerjalanan(int idPerjalanan, String currentRole);
	String getNotifTimelineByIdPerjalanan(int idPerjalanan);
}
