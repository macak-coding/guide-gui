package com.guideme.gui.service.inf;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import com.guideme.common.entity.Perjalanan;
import com.guideme.common.model.StatusResponse;
import com.guideme.gui.model.PageableWrapper;

public interface PerjalananServiceInf {
	PageableWrapper getPerjalananByIdGuideOrTravelerPageable(int idGuide, Pageable pageable, String role);
	StatusResponse getPerjalananByIdPerjalanan(int idPerjalanan);
}
