package com.guideme.gui.service.inf;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.guideme.common.entity.Kota;
import com.guideme.common.entity.UserDetail;
import com.guideme.gui.model.SearchGuide;

public interface SearchGuideServiceInf {
	public String composeCriteria(SearchGuide searchGuide);
	
	/**
	 * @param criteria
	 * @param pageable
	 * @return Page<String>
	 * 
	 * Nilai kembalian berupa list template html yang telah diproses freemarker
	 * Digunakan untuk search guide dari ajax request
	 */
	
	public Page<String> getListUserByCriteria(String criteria, Pageable pageable);
	
	/**
	 * @param criteria
	 * @param pageable
	 * @return Page<String>
	 * 
	 * Digunakan untuk search guide yang dilakukan melalui form
	 */
	
	public Page<UserDetail> getListUserByCriteriaForm(String criteria, Pageable pageable);
	
	public List<Kota> getKotaLike(String namaKota);
}
