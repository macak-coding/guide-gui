package com.guideme.gui.service.inf;

import com.guideme.common.model.StatusResponse;
import com.guideme.gui.model.UserDetailCust;

public interface TransaksiServiceInf {
	public StatusResponse getByIdTransaksi(String kodeTransaksi, UserDetailCust currentUser);
}
