package com.guideme.gui.service.inf;

import org.springframework.web.multipart.MultipartFile;

import com.guideme.common.entity.UserDetail;
import com.guideme.common.model.StatusResponse;

public interface UploadFileServiceInf<T> {
	T saveFile(MultipartFile multipartFile);
	StatusResponse uploadFotoProfil(MultipartFile multipartFile, UserDetail idUserDetail);
	StatusResponse uploadBuktiPembayaran(MultipartFile multipartFile, int idTransaksi);
	StatusResponse uploadVerifikasi(MultipartFile multipartFile, UserDetail idUserDetail, int flag);
}
