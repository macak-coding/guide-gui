package com.guideme.gui.service.inf;

import com.guideme.common.entity.User;

public interface UserServiceInf {
	String checkUsername(User user);
	String checkEmail(User user);
}
