package com.guideme.gui.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class UrlConstant {
	@Value("${core.server.hostname}")
	public static String CORE_URL; 
	
	public static final String URL_PERJALANAN_BY_ID_GUIDE = "/perjalanan/getPerjalananByIdGuidePage/";
	public static final String URL_PERJALANAN_BY_ID_TRAVELER = "/perjalanan/getPerjalananByIdTravelerPage/";
	public static final String URL_PERJALANAN_BY_ID_PERJALANAN = "/perjalanan/getPerjalananByIdPerjalanan/";
	
	public static final String URL_USER_CHECK_EMAIL = "/user/checkEmailExist";
	public static final String URL_USER_CHECK_USERNAME = "/user/checkUsernameExist";
	
	public static final String URL_NOTIFIKASI_BY_ID_PENERIMA_PAGEABLE = "/notifikasi/getByIdPenerimaPageable/";
	public static final String URL_NOTIFIKASI_BY_ID_PERJALANAN_PAGEABLE = "/notifikasi/getByIdPerjalananPageable/";
	public static final String URL_NOTIFIKASI_TIMELINE_BY_ID_PERJALANAN = "/notifikasiTimeline/getByIdPerjalanan/";
	
	public static final String URL_PROFIL_UPDATE_FOTO = "/user-detail/updateFotoProfilUrl";
	public static final String URL_PROFIL_UPDATE_FOTO_IDENTITAS = "/user-detail/updateFotoIdentitasUrl";
	
	public static final String URL_TRANSAKSI_BUKTI_PEMBAYARAN = "/transaksi/buktiPembayaran";
	public static final String URL_TRANSAKSI_BASE_URL = "/transaksi";
	public static final String URL_TRANSAKSI_BY_ID_PERJALANAN = "/transaksi/idPerjalanan/";
	
	public static final String URL_USER_DETAIL_CRITERIA = "/user-detail/findByCriteria?criteria=";
	
	public static final String URL_KOTA_GET_KOTA_LIKE = "/kota/getKota/";
	
	public static final String URL_JALAN_BARENG_GET_ALL_PAGEABLE = "/jalanBareng/getAllPageable/";
	public static final String URL_JALAN_BARENG_GET_BY_KOTA_LIKE = "/jalanBareng/getAllByKotaTujuan/";
	
	
	
}
