package com.guideme.gui.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.guideme.common.entity.User;
import com.guideme.gui.model.UserDetailCust;

@Component
public class UserUtil {
	
	public User getUserLoggedIn(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		UserDetailCust user = (UserDetailCust) auth.getPrincipal();
		
		return user;
	}
}
